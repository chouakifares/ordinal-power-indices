import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import seaborn as sns
from itertools import combinations, permutations
import pandas as pd
from tqdm import tqdm

"""
    function that creates a pair comparision matrix and
    displays it a s heatmap from a ranking given in input
"""
def ranking_to_heatmap(ranking, players):
    labels=[i.name for i in players]
    g = sns.heatmap(ranking,
        annot=True,
        fmt="d",
        cmap='YlGnBu',
        xticklabels=labels,
        yticklabels=labels,
        linewidths=.5)
    g.tick_params(axis='both', which='major', labelsize=8, labelbottom = False, bottom=False, top = True, labeltop=True)
    g.set_xticklabels(g.get_xticklabels(), fontsize = 8,rotation=90, horizontalalignment='right')
    g.set_yticklabels(g.get_yticklabels(), fontsize = 8,rotation=0, horizontalalignment='right')
    plt.show()

"""
    function that creates a graph of comparisions and
    from a ranking given in input
"""
def ranking_to_graph(ranking, players):
    g = nx.DiGraph()
    names = [i.name for i in players]
    g.add_nodes_from(names)
    for i in range(len(names)):
        for j in range(i+1,len(names)):
            if(ranking[i, j] == 1):
                g.add_edge(names[i], names[j])

            elif(ranking[i,j] == -1):
                g.add_edge(names[j], names[i])
    return g


"""
    function used to display a ranking on the command line
"""
def display_ranking(equivalence_classes, players):
    for i in range(len(equivalence_classes)):
        for j in range(len(equivalence_classes[i])):
            print(players[equivalence_classes[i][j]].name, sep="", end="")
            if(j < len(equivalence_classes[i]) -1):
                print(" ~ ", sep="", end="")
        if(i < len(equivalence_classes) -1):
            print(" > ", sep="", end="")
    print()

"""
    function that computes the kendalltau distance between two rankings
    args:
    ranking_a, ranking_b(1d_array): rankings to compute the distance between

    returns
        (int): distance between ranking_a and ranking_b
"""
def kendalltau_dist(ranking_a, ranking_b):
    tau = 0
    n_candidates = len(ranking_a)
    for i, j in combinations(range(n_candidates), 2):
        #in case two players are rabked the same in one ranking
        #and not the same in the other we increment tau by 1/2 instead of 1
        if ranking_a[i] == ranking_a[j]:
            tau += (ranking_b[i] != ranking_b[j])/2
        else:
            if(ranking_b[i] == ranking_b[j]):
                tau+= 1/2
            else:
                tau += np.sign(ranking_a[i] - ranking_a[j]) == -np.sign(ranking_b[i] - ranking_b[j])

    return 1 - 4*tau/(n_candidates*(n_candidates-1))

def spearman_ro(ranking_a, ranking_b, find_critical = False):
    ro = 0
    n_candidates = len(ranking_a)
    criticals = []
    for i in range(n_candidates):
        ro+= (ranking_a[i] - ranking_b[i])**2
        if(find_critical and np.abs((ranking_a[i] - ranking_b[i])) > 4):
            criticals.append(i)
    if(find_critical):
        return  (1- 6*ro/((n_candidates**2 - 1)*n_candidates), criticals)
    else:
        return 1- 6*ro/((n_candidates**2 - 1)*n_candidates)

def find_critical_case(team,reference_ranking,ranking, metric):
    if(metric(reference_ranking, ranking) <0.5):
        return spearman_ro(reference_ranking, ranking, True)[1]
    else:
        return []

"""
    Builds the graph needed for computing the kemeny young optimal rank aggregation
    returns
        (2d-array): adjacency matrix of the graph,
            the content of the cell [i, j] is the weight of the edge going from node i to node j
            if [i,j] < 0 then we set it to 0
"""
def build_graph(rankings):
    n_voters, n_candidates = rankings.shape
    edge_weights = np.zeros((n_candidates, n_candidates))
    for i, j in combinations(range(n_candidates), 2):
        preference = rankings[:, i] - rankings[:, j]
        h_ij = np.sum(preference < 0)
        h_ji = np.sum(preference > 0)
        if h_ij > h_ji:
            edge_weights[i, j] = h_ij - h_ji
        elif h_ij < h_ji:
            edge_weights[j, i] = h_ji - h_ij
    return edge_weights

def get_NBA_player_ranking(team, ranking_type):
    path =r'./NBA_player_rankings/{0}/{0}_{1}.csv'.format(team, ranking_type)
    df = pd.read_csv(path)
    ranking = []
    for row in range(df.shape[0]):
        ranking.append([df.iloc[row]["Unnamed: 1"]])
    return ranking
"""
    list that transforms a ranking to a voting ballot
    args:
    ranking([[str]]): list of lists where each list contains the name
    of all the players that are in the same equivalence classe
    base([str]): list of the alternative. It is used to represent
    the base ordering of all the alternatives in a voting ballot
"""
def ranking_to_ballot(ranking, base):
    ballot = np.zeros((len(base)), dtype=int)
    for i in range(len(base)):
        for j in range(len(ranking)):
            if base[i] in ranking[j]:
                ballot[i] = j
                break
    return ballot

def index_ranking_to_name_ranking(ranking, players):
    new_ranking = []
    for i in ranking:
        tmp = []
        for j in i:
            tmp.append(players[j].name)
        new_ranking.append(tmp)
    return new_ranking

def ranking_to_comp_matrix(ranking, players):
    comp_matrix = np.zeros((len(players), len(players)), dtype = int)
    for i in range(len(players)):
        passed = False
        for equiv_class in ranking:

            #if i in equivalence class then all the players in that equivalence class are indifferent to i
            if players[i] in equiv_class:
                passed = True
                for j in equiv_class:
                    comp_matrix[i,players.index(j)] = 0
            #if i not in equivalence class and we still didn't find i then all the players in that equivalence class are better than i
            elif passed:
                for j in equiv_class:
                    comp_matrix[i,players.index(j)] = 1
            else:
                for j in equiv_class:
                    comp_matrix[i,players.index(j)] = -1
    return comp_matrix
def bradley_terry(matrix, lineups , nb_iterations = 100, eps = 1e-5):
    p = np.random.rand(matrix.shape[0])
    old_p = np.array(p)
    old_p = old_p/np.linalg.norm(old_p)
    w = np.sum(matrix , axis = 1)
    iter = 0
    while iter < nb_iterations:
        for i in tqdm(range(p.shape[0])):
            denominator = 0
            j_values = [j for j in range(p.shape[0]) if j != i]
            for j in j_values:
                if((p[i]+p[j])):
                    denominator += (matrix[i,j] + matrix[j, i])/(p[i]+p[j])

            if(denominator):
                p[i] = w[i] / denominator
            else:
                p[i] = w[i]
        p = p/np.linalg.norm(p)
        print(np.sum(np.abs(p - old_p)))
        if(np.sum(np.abs(p - old_p)) < eps):
            break
        old_p = np.array(p)
        iter+=1

    equivalence_classes = {}
    sorted_lineups = np.argsort(p)
    zeros = []
    for i in range(len(p)):
        proba = p[i]
        if proba in equivalence_classes.keys():
            equivalence_classes[proba].append(lineups[i].lineup_id)
        else:
            equivalence_classes[proba] = [lineups[i].lineup_id]
    final_equiv_classes = []
    for k in reversed(sorted(equivalence_classes.keys())):
        final_equiv_classes.append(equivalence_classes[k])
    return final_equiv_classes
