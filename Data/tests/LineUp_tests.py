from context import session, cleanup_tables
import unittest
import datetime
import sys
sys.path.insert(0, '{0}/./..'.format(sys.path[0]))

from Model import Player, Team, LineUp, Confrontation

class InsertTest(unittest.TestCase):
    def test(self):
        cleanup_tables()
        #creating teams
        t1 = Team(team_name = "T1")
        #inserting the teams
        session.add(t1)
        session.commit()
        #creating the players
        p11 = Player(name = "p1 T1", team = t1.team_id)
        p12 = Player(name = "p2 T1", team = t1.team_id)
        p13 = Player(name = "p3 T1", team = t1.team_id)
        p14 = Player(name = "p4 T1", team = t1.team_id)
        p15 = Player(name = "p5 T1", team = t1.team_id)
        p16 = Player(name = "p6 T1", team = t1.team_id)

        session.add_all([p11, p12, p13, p14, p15, p16])
        session.commit()

        l1 = LineUp(p1 = p11.player_id, p2 = p12.player_id, p3 = p13.player_id, p4 = p14.player_id, p5 = p15.player_id)
        l2 = LineUp(p1 = p12.player_id, p2 = p11.player_id, p3 = p13.player_id, p4 = p15.player_id, p5 = p16.player_id)
        l3 = LineUp(p1 = p11.player_id, p2 = p12.player_id, p3 = p13.player_id, p4 = p15.player_id, p5 = p16.player_id)
        l4 = LineUp(p1 = p12.player_id, p2 = p11.player_id, p3 = p16.player_id, p4 = p14.player_id, p5 = p13.player_id)

        session.add_all([l1, l2, l3, l4])
        session.commit()

        lineups = session.query(LineUp).all()

        self.assertEqual(len(lineups), 4)


class SelectTest(unittest.TestCase):

    def test_select_all(self):
        cleanup_tables()
        #creating teams
        t1 = Team(team_name = "T1")
        #inserting the teams
        session.add(t1)
        session.commit()
        #creating the players
        p11 = Player(name = "p1 T1", team = t1.team_id)
        p12 = Player(name = "p2 T1", team = t1.team_id)
        p13 = Player(name = "p3 T1", team = t1.team_id)
        p14 = Player(name = "p4 T1", team = t1.team_id)
        p15 = Player(name = "p5 T1", team = t1.team_id)
        p16 = Player(name = "p6 T1", team = t1.team_id)

        session.add_all([p11, p12, p13, p14, p15, p16])
        session.commit()

        l1 = LineUp(p1 = p11.player_id, p2 = p12.player_id, p3 = p13.player_id, p4 = p14.player_id, p5 = p15.player_id)
        l2 = LineUp(p1 = p12.player_id, p2 = p11.player_id, p3 = p13.player_id, p4 = p15.player_id, p5 = p16.player_id)
        l3 = LineUp(p1 = p11.player_id, p2 = p12.player_id, p3 = p13.player_id, p4 = p15.player_id, p5 = p16.player_id)
        l4 = LineUp(p1 = p12.player_id, p2 = p11.player_id, p3 = p16.player_id, p4 = p14.player_id, p5 = p13.player_id)

        session.add_all([l1, l2, l3, l4])
        session.commit()

        lineups = session.query(LineUp).all()
        self.assertEqual(len(lineups), 4)


    def test_select_specific_lineup(self):
        cleanup_tables()
        #creating teams
        t1 = Team(team_name = "T1")
        #inserting the teams
        session.add(t1)
        session.commit()
        #creating the players
        p11 = Player(name = "p1 T1", team = t1.team_id)
        p12 = Player(name = "p2 T1", team = t1.team_id)
        p13 = Player(name = "p3 T1", team = t1.team_id)
        p14 = Player(name = "p4 T1", team = t1.team_id)
        p15 = Player(name = "p5 T1", team = t1.team_id)
        p16 = Player(name = "p6 T1", team = t1.team_id)

        session.add_all([p11, p12, p13, p14, p15, p16])
        session.commit()

        l1 = LineUp(p1 = p11.player_id, p2 = p12.player_id, p3 = p13.player_id, p4 = p14.player_id, p5 = p15.player_id)
        l2 = LineUp(p1 = p12.player_id, p2 = p11.player_id, p3 = p13.player_id, p4 = p15.player_id, p5 = p16.player_id)
        l3 = LineUp(p1 = p11.player_id, p2 = p12.player_id, p3 = p13.player_id, p4 = p15.player_id, p5 = p16.player_id)
        l4 = LineUp(p1 = p12.player_id, p2 = p11.player_id, p3 = p16.player_id, p4 = p14.player_id, p5 = p13.player_id)

        session.add_all([l1, l2, l3, l4])
        session.commit()

        l_true = session.query(LineUp).filter(
            LineUp.p1 == p11.player_id,
            LineUp.p2 == p12.player_id,
            LineUp.p3 == p13.player_id,
            LineUp.p4 == p14.player_id,
            LineUp.p5 == p15.player_id,
            ).all()
        self.assertEqual(len(l_true), 1)

        l_false = session.query(LineUp).filter(
            LineUp.p1 == p16.player_id,
            LineUp.p2 == p12.player_id,
            LineUp.p3 == p13.player_id,
            LineUp.p4 == p14.player_id,
            LineUp.p5 == p15.player_id,
            ).all()
        self.assertEqual(len(l_false), 0)


    def test_select_confrontations(self):
        cleanup_tables()

        t1 = Team(team_name = "T1")
        t2 = Team(team_name = "T2")

        session.add_all([t1,t2])
        session.commit()

        p11 = Player(name = "p1 T1", team = t1.team_id)
        p12 = Player(name = "p2 T1", team = t1.team_id)
        p13 = Player(name = "p3 T1", team = t1.team_id)
        p14 = Player(name = "p4 T1", team = t1.team_id)
        p15 = Player(name = "p5 T1", team = t1.team_id)
        p16 = Player(name = "p6 T1", team = t1.team_id)

        p21 = Player(name = "p1 T2", team = t2.team_id)
        p22 = Player(name = "p2 T2", team = t2.team_id)
        p23 = Player(name = "p3 T2", team = t2.team_id)
        p24 = Player(name = "p4 T2", team = t2.team_id)
        p25 = Player(name = "p5 T2", team = t2.team_id)
        p26 = Player(name = "p6 T2", team = t2.team_id)


        session.add_all([p11, p12, p13, p14, p15, p16, p21, p22, p23, p24, p25, p26])
        session.commit()

        l11 = LineUp(p1 = p11.player_id, p2 = p12.player_id, p3 = p13.player_id, p4 = p14.player_id, p5 = p15.player_id)
        l12 = LineUp(p1 = p12.player_id, p2 = p11.player_id, p3 = p13.player_id, p4 = p15.player_id, p5 = p16.player_id)
        l13 = LineUp(p1 = p11.player_id, p2 = p12.player_id, p3 = p13.player_id, p4 = p15.player_id, p5 = p16.player_id)
        l14 = LineUp(p1 = p12.player_id, p2 = p11.player_id, p3 = p16.player_id, p4 = p14.player_id, p5 = p13.player_id)

        l21 = LineUp(p1 = p21.player_id, p2 = p22.player_id, p3 = p23.player_id, p4 = p24.player_id, p5 = p25.player_id)
        l22 = LineUp(p1 = p22.player_id, p2 = p21.player_id, p3 = p23.player_id, p4 = p25.player_id, p5 = p26.player_id)

        session.add_all([l11, l12, l13, l14, l21, l22])
        session.commit()


        c1 = Confrontation(l1 = l11.lineup_id, l2 = l21.lineup_id, score = 4, match_date = datetime.datetime(2010, 10,10), start_time_m = 1 , start_time_s = 10, end_time_m = 0, end_time_s = 33, quarter = 3)
        c2 = Confrontation(l1 = l11.lineup_id, l2 = l22.lineup_id, score = 5, match_date = datetime.datetime(2010, 10,26), start_time_m = 2 , start_time_s = 43, end_time_m = 1, end_time_s = 56, quarter = 2)
        c3 = Confrontation(l1 = l12.lineup_id, l2 = l21.lineup_id, score = 9, match_date = datetime.datetime(2010, 6,1), start_time_m = 1 , start_time_s = 56, end_time_m = 1, end_time_s = 38, quarter = 1)
        c4 = Confrontation(l1 = l12.lineup_id, l2 = l22.lineup_id, score = -4, match_date = datetime.datetime(2010, 11,9), start_time_m = 9 , start_time_s = 42, end_time_m =9, end_time_s = 27, quarter = 2)
        c5 = Confrontation(l1 = l13.lineup_id, l2 = l21.lineup_id, score = 9, match_date = datetime.datetime(2010, 12,22), start_time_m = 7 , start_time_s = 19, end_time_m = 6, end_time_s = 22, quarter = 4)
        c6 = Confrontation(l1 = l13.lineup_id, l2 = l22.lineup_id, score = 1, match_date = datetime.datetime(2010, 1,19), start_time_m = 9 , start_time_s = 53, end_time_m = 7, end_time_s = 59, quarter = 1)
        c7 = Confrontation(l1 = l14.lineup_id, l2 = l21.lineup_id, score = 10, match_date = datetime.datetime(2010, 3,7), start_time_m = 5 , start_time_s = 25, end_time_m = 4, end_time_s = 54, quarter = 2)
        c8 = Confrontation(l1 = l14.lineup_id, l2 = l22.lineup_id, score = -7, match_date = datetime.datetime(2010, 4,6), start_time_m = 3 , start_time_s = 18, end_time_m = 2, end_time_s = 45, quarter = 3)

        session.add_all([c1,c2,c3,c4,c5,c6,c7,c8])
        session.commit()


        self.assertEqual(len(l11.confrontations(session)), 2)
        self.assertEqual(len(l12.confrontations(session)), 2)
        self.assertEqual(len(l13.confrontations(session)), 2)
        self.assertEqual(len(l14.confrontations(session)), 2)
        self.assertEqual(len(l21.confrontations(session)), 4)
        self.assertEqual(len(l22.confrontations(session)), 4)

        for c in l11.confrontations(session):
            self.assertTrue(l11.lineup_id in c.lineups())
        for c in l12.confrontations(session):
            self.assertTrue(l12.lineup_id in c.lineups())
        for c in l13.confrontations(session):
            self.assertTrue(l13.lineup_id in c.lineups())
        for c in l14.confrontations(session):
            self.assertTrue(l14.lineup_id in c.lineups())
        for c in l21.confrontations(session):
            self.assertTrue(l21.lineup_id in c.lineups())
        for c in l22.confrontations(session):
            self.assertTrue(l22.lineup_id in c.lineups())


if __name__ == "__main__":
    unittest.main()
