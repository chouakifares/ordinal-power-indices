from sqlalchemy.orm import sessionmaker
from sqlalchemy import  create_engine
import sys
sys.path.insert(0, '{0}/./..'.format(sys.path[0]))
sys.path.insert(0, '{0}/./..'.format(sys.path[0]))


from Model import Base, Player, Team, LineUp, Confrontation


def cleanup_tables():
    session.query(Team).delete()
    session.query(Player).delete()
    session.query(LineUp).delete()
    session.query(Confrontation).delete()


engine = create_engine("sqlite:///{0}.db".format("TESTING_DATABASE"),echo = False)


Base.metadata.drop_all(bind=engine)
Base.metadata.create_all(bind = engine)

#Establishing connection to database
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()
