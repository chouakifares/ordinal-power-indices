from context import session, cleanup_tables
import unittest

import sys
sys.path.insert(0, '{0}/./..'.format(sys.path[0]))

from Model import Player, Team, LineUp




class InsertTest(unittest.TestCase):
    def test(self):
        cleanup_tables()
        #creating teams
        t1 = Team(team_name = "T1")
        t2 = Team(team_name = "T2")
        #inserting the teams
        session.add_all([t1,t2])
        session.commit()
        #creating the players
        p11 = Player(name = "p1 T1", team = t1.team_id)
        p21 = Player(name = "p1 T2", team = t2.team_id)
        #inserting the players
        session.add_all([p11, p21])
        session.commit()
        players = session.query(Player).all()
        teams = session.query(Team).all()

        self.assertEqual(len(players), 2)
        self.assertEqual(players[0].name, "p1 T1")
        self.assertEqual(players[1].name, "p1 T2")

        self.assertEqual(players[0].team, teams[0].team_id)
        self.assertEqual(players[1].team, teams[1].team_id)


class SelectTest(unittest.TestCase):

    def test_select_all(self):
        cleanup_tables()
        #creating teams
        t1 = Team(team_name = "T1")
        t2 = Team(team_name = "T2")
        #inserting the teams
        session.add_all([t1,t2])
        session.commit()
        #creating the players
        p11 = Player(name = "p1 T1", team = t1.team_id)
        p21 = Player(name = "p1 T2", team = t2.team_id)
        #inserting the players
        session.add_all([p11, p21])
        session.commit()
        players = session.query(Player).all()
        self.assertEqual(len(players), 2)

    def test_select_same_team_players(self):
        cleanup_tables()
        #creating teams
        t1 = Team(team_name = "T1")
        #inserting the teams
        session.add(t1)
        session.commit()
        #creating the players
        p11 = Player(name = "p1 T1", team = t1.team_id)
        p12 = Player(name = "p2 T1", team = t1.team_id)
        p13 = Player(name = "p3 T1", team = t1.team_id)
        p14 = Player(name = "p4 T1", team = t1.team_id)
        p15 = Player(name = "p5 T1", team = t1.team_id)
        p16 = Player(name = "p6 T1", team = t1.team_id)

        #inserting the players
        session.add_all([p11, p12, p13, p14, p15, p16])
        session.commit()
        players = session.query(Player).filter(Player.team == t1.team_id).all()
        self.assertEqual(len(players), 6)
        for p in players:
            self.assertEqual(p.team , t1.team_id)

    def test_select_lineups(self):
        cleanup_tables()
        #creating teams
        t1 = Team(team_name = "T1")
        #inserting the teams
        session.add(t1)
        session.commit()

        p11 = Player(name = "p1 T1", team = t1.team_id)
        p12 = Player(name = "p2 T1", team = t1.team_id)
        p13 = Player(name = "p3 T1", team = t1.team_id)
        p14 = Player(name = "p4 T1", team = t1.team_id)
        p15 = Player(name = "p5 T1", team = t1.team_id)
        p16 = Player(name = "p6 T1", team = t1.team_id)

        session.add_all([p11, p12, p13, p14, p15, p16])
        session.commit()


        l1 = LineUp(p1 = p11.player_id, p2 = p12.player_id, p3 = p13.player_id, p4 = p14.player_id, p5 = p15.player_id)
        l2 = LineUp(p1 = p12.player_id, p2 = p11.player_id, p3 = p13.player_id, p4 = p15.player_id, p5 = p16.player_id)
        l3 = LineUp(p1 = p11.player_id, p2 = p12.player_id, p3 = p13.player_id, p4 = p15.player_id, p5 = p16.player_id)
        l4 = LineUp(p1 = p12.player_id, p2 = p11.player_id, p3 = p16.player_id, p4 = p14.player_id, p5 = p13.player_id)

        session.add_all([l1, l2, l3, l4])
        session.commit()

        self.assertEqual(len(p11.lineups(session)),4)
        for l in p11.lineups(session):
            self.assertTrue(p11.player_id in l.players().values())

        self.assertEqual(len(p12.lineups(session)),4)
        for l in p12.lineups(session):
            self.assertTrue(p12.player_id in l.players().values())

        self.assertEqual(len(p13.lineups(session)),4)
        for l in p13.lineups(session):
            self.assertTrue(p13.player_id in l.players().values())

        self.assertEqual(len(p14.lineups(session)),2)
        for l in p14.lineups(session):
            self.assertTrue(p14.player_id in l.players().values())

        self.assertEqual(len(p15.lineups(session)),3)
        for l in p15.lineups(session):
            self.assertTrue(p15.player_id in l.players().values())

        self.assertEqual(len(p16.lineups(session)),3)
        for l in p16.lineups(session):
            self.assertTrue(p16.player_id in l.players().values())



if __name__ == "__main__":
    unittest.main()
