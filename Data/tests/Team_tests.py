from context import session , cleanup_tables
import sys
import unittest

sys.path.insert(0, '{0}/./..'.format(sys.path[0]))
from Model import Player, Team, LineUp


class InsertTest(unittest.TestCase):

    def test(self):
        cleanup_tables()
        #creating teams
        t1 = Team(team_name = "T1")
        t2 = Team(team_name = "T2")
        #inserting the teams
        session.add_all([t1,t2])
        session.commit()

        teams = session.query(Team).all()

        self.assertEqual(len(teams), 2)

        self.assertEqual(teams[0].team_name, "T1")
        self.assertEqual(teams[1].team_name, "T2")


class SelectTest(unittest.TestCase):

    def test_select_all(self):
        cleanup_tables()
        #creating teams
        t1 = Team(team_name = "T1")
        t2 = Team(team_name = "T2")
        #inserting the teams
        session.add_all([t1,t2])
        session.commit()

        teams = session.query(Team).all()
        self.assertEqual(len(teams), 2)

    def test_select_specific(self):
        cleanup_tables()
        #creating teams
        t1 = Team(team_name = "T1")
        t2 = Team(team_name = "T2")
        #inserting the teams
        session.add_all([t1,t2])
        session.commit()

        teams = session.query(Team).filter(Team.team_name =="T1").all()
        self.assertEqual(len(teams), 1)
        self.assertEqual(teams[0].team_name, "T1")

    def test_select_players_team(self):
        cleanup_tables()

        t1 = Team(team_name = "T1")
        t2 = Team(team_name = "T2")

        session.add_all([t1,t2])
        session.commit()

        p11 = Player(name = "p1 T1", team = t1.team_id)
        p12 = Player(name = "p2 T1", team = t1.team_id)
        p13 = Player(name = "p3 T1", team = t1.team_id)
        p14 = Player(name = "p4 T1", team = t1.team_id)
        p15 = Player(name = "p5 T1", team = t1.team_id)
        p16 = Player(name = "p6 T1", team = t1.team_id)

        p21 = Player(name = "p1 T2", team = t2.team_id)
        p22 = Player(name = "p2 T2", team = t2.team_id)
        p23 = Player(name = "p3 T2", team = t2.team_id)
        p24 = Player(name = "p4 T2", team = t2.team_id)
        p25 = Player(name = "p5 T2", team = t2.team_id)
        p26 = Player(name = "p6 T2", team = t2.team_id)


        session.add_all([p11, p12, p13, p14, p15, p16, p21, p22, p23, p24, p25, p26])
        session.commit()

        players_t1 = t1.players(session)
        players_t2 = t2.players(session)

        self.assertEqual(len(players_t1), 6)
        self.assertEqual(len(players_t1), len(players_t2))

        for i in range(len(players_t1)):
            self.assertEqual(players_t1[i].team, t1.team_id)
            self.assertEqual(players_t2[i].team, t2.team_id)

    def test_select_lineups(self):
        cleanup_tables()

        t1 = Team(team_name = "T1")
        t2 = Team(team_name = "T2")

        session.add_all([t1,t2])
        session.commit()

        p11 = Player(name = "p1 T1", team = t1.team_id)
        p12 = Player(name = "p2 T1", team = t1.team_id)
        p13 = Player(name = "p3 T1", team = t1.team_id)
        p14 = Player(name = "p4 T1", team = t1.team_id)
        p15 = Player(name = "p5 T1", team = t1.team_id)
        p16 = Player(name = "p6 T1", team = t1.team_id)

        p21 = Player(name = "p1 T2", team = t2.team_id)
        p22 = Player(name = "p2 T2", team = t2.team_id)
        p23 = Player(name = "p3 T2", team = t2.team_id)
        p24 = Player(name = "p4 T2", team = t2.team_id)
        p25 = Player(name = "p5 T2", team = t2.team_id)
        p26 = Player(name = "p6 T2", team = t2.team_id)


        session.add_all([p11, p12, p13, p14, p15, p16, p21, p22, p23, p24, p25, p26])
        session.commit()

        l11 = LineUp(p1 = p11.player_id, p2 = p12.player_id, p3 = p13.player_id, p4 = p14.player_id, p5 = p15.player_id)
        l12 = LineUp(p1 = p12.player_id, p2 = p11.player_id, p3 = p13.player_id, p4 = p15.player_id, p5 = p16.player_id)
        l13 = LineUp(p1 = p11.player_id, p2 = p12.player_id, p3 = p13.player_id, p4 = p15.player_id, p5 = p16.player_id)
        l14 = LineUp(p1 = p12.player_id, p2 = p11.player_id, p3 = p16.player_id, p4 = p14.player_id, p5 = p13.player_id)

        l21 = LineUp(p1 = p21.player_id, p2 = p22.player_id, p3 = p23.player_id, p4 = p24.player_id, p5 = p25.player_id)
        l22 = LineUp(p1 = p22.player_id, p2 = p21.player_id, p3 = p23.player_id, p4 = p25.player_id, p5 = p26.player_id)


        session.add_all([l11, l12, l13, l14, l21, l22])
        session.commit()

        lineups_t1 = t1.lineups(session)
        lineups_t2 = t2.lineups(session)
        self.assertEqual(len(lineups_t1), 4)
        self.assertEqual(len(lineups_t2), 2)


        for l in lineups_t1:
            for p in l.players().values():
                tmp = session.query(Player).filter(Player.player_id == p).all()
                self.assertEqual(len(tmp), 1)
                self.assertEqual(tmp[0].team, t1.team_id)

        for l in lineups_t2:
            for p in l.players().values():
                tmp = session.query(Player).filter(Player.player_id == p).all()
                self.assertEqual(len(tmp), 1)
                self.assertEqual(tmp[0].team, t2.team_id)

if __name__ == "__main__":
    unittest.main()
