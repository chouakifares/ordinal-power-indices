import numpy as np
import pandas as pd
import os
from tqdm import tqdm
from Model import Player, LineUp
from utils import *

#function that tranforms a ranking to a dataframe
def ranking_to_df(ranking, players= None):
    if(players is not None):
        data = {"Players":[]}
        for i in ranking:
            data["Players"].append(" ~ ".join([players[j].name for j in i]))
    else:
        data = {"Players":[]}
        for i in ranking:
            data["Players"].append(" ~ ".join([j for j in i]))

    return pd.DataFrame(data = data)

#function that adds a style to a dataframe that contains a ranking.
def make_pretty_df(df):
    s = df.style
    cell_hover = {  # for row hover use <tr> instead of <td>
        'selector': 'td:hover',
        'props': [('background-color', '#22ffb3AA')]
    }
    index_names = {
        'selector': '.index_name',
        'props': 'font-style: italic; color: darkgrey; font-weight:normal;'
    }
    headers = {
        'selector': 'th:not(.index_name)',
        'props': 'background-color: #220066DD; color: white;'
    }
    s.set_table_styles([cell_hover, index_names, headers])
    s.set_table_styles([
        {'selector': 'th.col_heading', 'props': 'text-align: center;'},
        {'selector': 'th.col_heading.level0', 'props': 'font-size: 1.5em;'},
        {'selector': 'td', 'props': 'text-align: center; font-weight: bold;border-left:1px solid #000000'},
        {'selector': 'th', 'props': 'border-right: 1px solid #000000;'}
    ], overwrite=False)

    return s


#function that computes the distances between each ranking technique and the reference rankings and averages the distance over all the teams
def distance_matrix(session, teams, approach, time_criterion= False, fallback=None, bradley_terry = False):
    NB_RANKINGS = 11
    nb_teams = 30
    distance_matrix_kendall = np.zeros((NB_RANKINGS, NB_RANKINGS))
    distance_matrix_spearman = np.zeros((NB_RANKINGS, NB_RANKINGS))
    for id_ in tqdm(range(len(teams))):
        # for teams with these ids the TOPCYCLE method takes too much time 
        if(id_ == 27 or id_==19):

            nb_teams -= 1
            continue
        t = teams[id_]
        players = t.players(session)
        VORP_ranking = get_NBA_player_ranking(teams[id_].team_name, "VORP")
        pts_ranking = get_NBA_player_ranking(teams[id_].team_name, "Points")
        WAR_ranking = get_NBA_player_ranking(teams[id_].team_name, "WAR")
        raptor_ranking = get_NBA_player_ranking(teams[id_].team_name, "RAPTOR")
        time_ranking = t.time_based_player_ranking(session)
        base_nba = sorted([i[0] for i in VORP_ranking])
        base_db = sorted([i.name for i in players])
        rankings = [
            ranking_to_ballot(VORP_ranking, base_nba),
            ranking_to_ballot(pts_ranking, base_db),
            ranking_to_ballot(WAR_ranking, base_db),
            ranking_to_ballot(raptor_ranking, base_db),
            ranking_to_ballot(time_ranking, base_db)
        ]
        nb_players = len(players)

        #ks_cp(6)
        rankings.append(ranking_to_ballot(
            index_ranking_to_name_ranking(
                t.krammer_simpson(
                    session,
                    Player.ceteris_paribus,
                    approach,
                    time_criterion = time_criterion,
                    penality = True,
                    show_heatmap = False,
                    show_progress = False,
                    fallback = fallback,
                    bradley_terry = bradley_terry
                ),
                players
            ),
            base_db
        ))

        #cop_cp (7)
        rankings.append(ranking_to_ballot(
            index_ranking_to_name_ranking(
                t.copeland(
                    session,
                    Player.ceteris_paribus,
                    approach,
                    penality=True,
                    time_criterion = time_criterion,
                    show_heatmap= False,
                    show_progress = False,
                    fallback = fallback,
                    bradley_terry = bradley_terry
                ),
                players
            ),
            base_db
        ))
        #(cycle => equiv)_cp (8)
        rankings.append(ranking_to_ballot(
            index_ranking_to_name_ranking(
                t.connected_components_equiv(
                    session,
                    Player.ceteris_paribus,
                    approach,
                    penality=True,
                    time_criterion = time_criterion,
                    show_heatmap= False,
                    show_progress = False,
                    fallback = fallback,
                    bradley_terry = bradley_terry
                ),
                players
            ),
            base_db
        ))
        #lex (9)
        rankings.append(ranking_to_ballot(
            index_ranking_to_name_ranking(
                t.rank_players(
                    session,
                    Player.l5_star,
                    approach,
                    time_criterion = time_criterion,
                    show_heatmap= False,
                    show_progress = False,
                    fallback = fallback,
                    bradley_terry = bradley_terry
                ),
                players
            ),
            base_db
        ))

        #borda(10)
        rankings.append(ranking_to_ballot(
            index_ranking_to_name_ranking(
                t.borda_ranking(
                    session,
                    approach,
                    time_criterion,
                    fallback,
                    bradley_terry = bradley_terry
                ),
                players
            ),
            base_db
        ))

        #stv(11)
        rankings.append(ranking_to_ballot(
            index_ranking_to_name_ranking(
                t.stv_based_ranking(
                    session,
                    approach,
                    time_criterion,
                    fallback,
                    bradley_terry = bradley_terry
                ),
                players
            ),
            base_db
        ))
        for i in range(NB_RANKINGS):
            distance_matrix_kendall[i, i] = 1
            distance_matrix_spearman[i, i] = 1
            for j in range(i+1, NB_RANKINGS):
                distance_matrix_kendall[i, j] += kendalltau_dist(rankings[i], rankings[j])/nb_teams
                distance_matrix_kendall[j, i] = distance_matrix_kendall[i, j]
                distance_matrix_spearman[i, j] += spearman_ro(rankings[i], rankings[j])/nb_teams
                distance_matrix_spearman[j, i] = distance_matrix_spearman[i, j]
    return (np.round(distance_matrix_kendall, 4), np.round(distance_matrix_spearman, 4))
