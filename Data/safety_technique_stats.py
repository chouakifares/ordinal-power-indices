"""
File that defines the necessary functions and main code
to implement and benchmark the comparisons with safety technique
"""

from sys import argv
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from Model import Player, Team, Confrontation, LineUp
from utils import *
from sklearn.linear_model import LinearRegression
from distance import ranking_to_df, distance_matrix, get_distance_df
import matplotlib.pyplot as plt
from tqdm import tqdm


# function that computes the % of comparaisions done when using the safety technic
def average_number_comp(session, approach, time_criterion, fallback):
    teams = session.query(Team).all()
    data = []
    for i in tqdm(range(30)):
        ranking_ks_cp_scoring = teams[i].krammer_simpson(session,
            Player.ceteris_paribus,
            approach,
            time_criterion,
            penality=True,
            fallback= fallback,
            show_heatmap=False,
            show_progress = False)
        ranking_copeland_cp_scoring = teams[i].copeland(session,
            Player.ceteris_paribus,
            approach,
            time_criterion,
            penality=True,
            fallback= fallback,
            show_heatmap=False,
            show_progress = False)
        ranking_equiv_cycles_cp_scoring = teams[i].connected_components_equiv(session,
            Player.ceteris_paribus,
            approach,
            time_criterion,
            penality=True,
            fallback= fallback,
            show_heatmap=False,
            show_progress = False)
        ranking_l5_star_scoring = teams[i].rank_players(session,
            Player.l5_star,
            approach,
            time_criterion,
            fallback,
            show_heatmap= False,
            show_progress = False)
        ranking_borda_scoring = teams[i].borda_ranking(session, approach, time_criterion, fallback)
        ranking_stv_scoring = teams[i].stv_based_ranking(session, approach, time_criterion, fallback)
        matrix = teams[i].safety_comparaision(session,
            [
                ranking_borda_scoring,
                ranking_ks_cp_scoring,
                ranking_copeland_cp_scoring,
                ranking_equiv_cycles_cp_scoring,
                ranking_l5_star_scoring,
                ranking_stv_scoring
            ]
        )

        data.append(1 - len(np.where(matrix == 2)[0])/(matrix.shape[0]*(matrix.shape[0]-1)))

    return data

def safety_vs_nba(session , approach, time_criterion, fallback , reference):
    teams = session.query(Team).all()
    data = []
    for i in tqdm(range(30)):
        ranking_nba = get_NBA_player_ranking(teams[i].team_name, reference)
        ranking_ks_cp_scoring = teams[i].krammer_simpson(session,
            Player.ceteris_paribus,
            approach,
            time_criterion,
            penality=True,
            fallback= fallback,
            show_heatmap=False,
            show_progress = False)
        ranking_copeland_cp_scoring = teams[i].copeland(session,
            Player.ceteris_paribus,
            approach,
            time_criterion,
            penality=True,
            fallback= fallback,
            show_heatmap=False,
            show_progress = False)
        ranking_equiv_cycles_cp_scoring = teams[i].connected_components_equiv(session,
            Player.ceteris_paribus,
            approach,
            time_criterion,
            penality=True,
            fallback= fallback,
            show_heatmap=False,
            show_progress = False)
        ranking_l5_star_scoring = teams[i].rank_players(session,
            Player.l5_star,
            approach,
            time_criterion,
            fallback,
            show_heatmap= False,
            show_progress = False)
        ranking_borda_scoring = teams[i].borda_ranking(session, approach, time_criterion, fallback)
        ranking_stv_scoring = teams[i].stv_based_ranking(session, approach, time_criterion, fallback)


        matrix = teams[i].safety_comparaision(session,
            [
                ranking_borda_scoring,
                ranking_ks_cp_scoring,
                ranking_copeland_cp_scoring,
                ranking_equiv_cycles_cp_scoring,
                ranking_l5_star_scoring,
                ranking_stv_scoring

            ]
        )
        nba_comp_matrix = ranking_to_comp_matrix(ranking_nba, [i.name for i in teams[i].players(session)])
        unusables = np.where(matrix == 2)
        total_pair_comp = (matrix.shape[0]*(matrix.shape[0]-1))
        nba_comp_matrix[unusables] = 0
        matrix[unusables] = 0
        data.append(
            (
                1 - len(unusables[0])/total_pair_comp,
                np.sum(matrix!=nba_comp_matrix)/(total_pair_comp - len(unusables[0]))
            )
        )
    return data
#function that measures how accurate the comparisons made using the safety technic
#are with respect to the reference rankings 
def measure_safety_accuracy(session, approach, time_criterion, fallback):
    df = {}
    refs = ["Points","WAR", "VORP","RAPTOR"]
    teams = session.query(Team).all()
    for r in refs:
        d = np.array(safety_vs_nba(session, approach, time_criterion, fallback, r))
        df[r] = {
            "% total comp" : np.mean(d[:,0]),
            "std var total comp" : np.std(d[:,0]),
            "% different" : np.mean(d[:,1]),
            "std var different" : np.std(d[:,1]),
        }
    return df
if __name__ == "__main__":
    engine = create_engine("sqlite:///{0}".format(argv[1]),echo = False)

    Session = sessionmaker()
    Session.configure(bind=engine)

    global session
    session = Session()

    safety_vs_nba(session, "scoring", False, None, "WAR")
