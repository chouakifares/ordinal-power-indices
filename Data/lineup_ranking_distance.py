from pickle import load
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sys import argv
from Model import Player, Team, Confrontation, LineUp
from utils import *
import seaborn as sns
def display_heatmap(matrix, index):
    g = sns.heatmap(matrix,annot=True,xticklabels=index,yticklabels=index,linewidths=.5, fmt=".3f", cmap='Blues')
    g.tick_params(axis='both', which='major', labelsize=8, labelbottom = False, bottom=False, top = True, labeltop=True)
    g.set_xticklabels(g.get_xticklabels(), fontsize = 8,rotation=90, horizontalalignment='right')
    g.set_yticklabels(g.get_yticklabels(), fontsize = 8,rotation=0, horizontalalignment='right')
    plt.show()
engine = create_engine("sqlite:///{0}".format(argv[1]),echo = False)

Session = sessionmaker()
Session.configure(bind=engine)

session = Session()
teams = session.query(Team).all()

NB_RANKINGS = 5
nb_teams = 30
distance_matrix_kendall = np.zeros((NB_RANKINGS, NB_RANKINGS))
distance_matrix_spearman = np.zeros((NB_RANKINGS, NB_RANKINGS))
for i in tqdm(range(len(teams))):
    t = teams[i]
    rankings, base = [],[]
    tmp = t.lineup_ranking(session, "scoring", False, None, False)
    tmp_ranking = []
    for i in tmp.keys():
        tmp_ranking.append(tmp[i])
        for j in tmp[i]:
            base.append(j)
    base = sorted(base)
    rankings.append(ranking_to_ballot(tmp_ranking,base))
    tmp = t.lineup_ranking(session, "pareto_dominance", False, None, False)
    tmp_ranking = []
    for i in tmp.keys():
        tmp_ranking.append(tmp[i])
    rankings.append(ranking_to_ballot(tmp_ranking,base))

    tmp = t.lineup_ranking(session, "pareto_dominance", True, None, False)
    tmp_ranking = []
    for i in tmp.keys():
        tmp_ranking.append(tmp[i])
    rankings.append(ranking_to_ballot(tmp_ranking,base))

    tmp = t.lineup_ranking(session, "witness", False, "scoring", False)
    tmp_ranking = []
    for i in tmp.keys():
        tmp_ranking.append(tmp[i])
    rankings.append(ranking_to_ballot(tmp_ranking,base))

    tmp = t.lineup_ranking(session, "witness", False, "scoring", True)
    tmp_ranking = []
    for i in tmp.keys():
        tmp_ranking.append(tmp[i])
    rankings.append(ranking_to_ballot(tmp_ranking,base))

    for i in range(len(rankings)):
        distance_matrix_kendall[i, i] = 1
        distance_matrix_spearman[i, i] = 1
        for j in range(i+1,len(rankings)):
            distance_matrix_kendall[i, j] += kendalltau_dist(rankings[i], rankings[j])/len(teams)
            distance_matrix_kendall[j, i] = distance_matrix_kendall[i, j]
            distance_matrix_spearman[i, j] += spearman_ro(rankings[i], rankings[j])/len(teams)
            distance_matrix_spearman[j, i] = distance_matrix_spearman[i, j]
index = ["scoring", "pareto dominance without time", "pareto dominance with time","witness without BT", "witness with BT"]

display_heatmap(np.round(distance_matrix_kendall, 4), index)

display_heatmap(np.round(distance_matrix_spearman, 4), index)
