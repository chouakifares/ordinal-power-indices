from sqlalchemy import Column, Integer, String, ForeignKey, Table, create_engine, DateTime, Time, or_, and_, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import UniqueConstraint
from tqdm import tqdm
from pickle import dump, load
from itertools import permutations, combinations
from functools import partial
import numpy as np
from utils import ranking_to_heatmap, ranking_to_graph, ranking_to_ballot, index_ranking_to_name_ranking, find_critical_case, kendalltau_dist, bradley_terry
import pandas as pd
from networkx import shortest_path, has_path
import networkx as nx
import matplotlib.pyplot as plt
Base = declarative_base()




"""
Class used to represent the Player table and manipulate player entites.
A Player is defined with the fields:
id (int): unique id of the player
name (str): name of the player
team (Team): the team to which the player plays during the current season
lineups ([LineUp]): list of lineups to which the player belongs i.e. all the lineups in which the player has played in the current season
"""

class Player(Base):
    __tablename__ = "player"
    player_id = Column(Integer, primary_key=True)
    name = Column(String)
    team = Column(Integer, ForeignKey("team.team_id"))

    """
    Static method that implement ceteris paribus to compare two players
    args:
    p1(Player): first player to compare
    p2(Player): second player to compare
    session(sqlalchemy.orm.Session): session that allows connection to the database containing dataset
    approach(str): approach used to compare the lineups can be scoring , pareto_dominance , witness
    time_criterion(bool): wether to use time as a criterion when using the pareto domainance approach or not
    penality(bool): Assume if this boolean is set to true if some coalition C is unusable because only p1 (or p2)  played with it
        then p2 (the player that stops the ceteris paribus procedure from using this coalition) is penalized by counting the confrontation
        between the C U p2 as a loser against C U p1.
    krammer_simpson(bool): in case we're interested in breaking the cycles, indicated if we're using the krammer simpson method
    lineup_ranking([lineUp]): ranking of lineups, if thie ranking is provided the comparaision between lineups
        will be done using this ranking insteaed of using the LineUp.compare method
    """
    @staticmethod
    def ceteris_paribus(
        p1,
        p2,
        session,
        approach,
        time_criterion= False,
        penality= True,
        krammer_simpson =False,
        fallback = "scoring",
        lineup_ranking= None):
        lineups_p1 = p1.lineups(session)
        lineups_p2 = p2.lineups(session)
        player_lp2, player_lp1_permut, player_lp1 =  [], [], []
        id1, id2 = [], []
        #look for all the lineups of p1 where p2 doesn't belong
        for id in range(len(lineups_p1)):
            l1 = lineups_p1[id]
            tmp = [i for i in l1.players().values()]
            if p2.player_id not in tmp:
                tmp.remove(p1.player_id)
                player_lp1.append(tmp)
                #permutation trick allows to consider all coalitions that contains the same players but don't play the same positions
                for i in permutations(tmp):
                    player_lp1_permut.append(list(i))
                    id1.append(id)
        #look for all the lineups of p2 where p1 doesn't exist
        for id in range(len(lineups_p2)):
            l2 = lineups_p2[id]
            tmp = [i for i in l2.players().values()]
            if p1.player_id not in tmp:
                tmp.remove(p2.player_id)
                player_lp2.append(tmp)
                id2.append(id)
        #use only the common lineups to compare
        common = [(player_lp1_permut.index(i),player_lp2.index(i)) for i in player_lp1_permut if i in player_lp2]
        #applying the penality
        if(penality):
            counter = len([i for i in player_lp1 if i not in common]) - len([i for i in player_lp2 if i not in common])
        else:
            counter = 0
        if(lineup_ranking is None):
            #in case we're not using a predefined ranking we use the static method compare of the Lineup
            for i in common:
                w = LineUp.compare(lineups_p1[id1[i[0]]], lineups_p2[id2[i[1]]], session, approach, time_criterion, fallback)
                if(w == 2):
                    counter -= 1
                else:
                    counter += 1
        else:
            #otherwise we use the ranking provided
            for i in common:
                rank_l1, rank_l2 = None , None
                for j in lineup_ranking.keys():
                    if(lineups_p1[id1[i[0]]].lineup_id in lineup_ranking[j]):
                        rank_l1 = j
                    if(lineups_p2[id2[i[1]]].lineup_id in lineup_ranking[j]):
                        rank_l2 = j
                    if(rank_l1 is not None and rank_l2 is not None):
                        break
                if(rank_l1 < rank_l2 ):
                    counter += 1
                elif(rank_l1 > rank_l2 ):
                    counter -= 1
        #when using the krammer simpson methos we need the value of the comparaisions not just which player wins
        if krammer_simpson:
            return counter
        else:
            return 0 if counter == 0 else p1 if counter > 0 else p2

    """
    Static method that implement round robin to compare two players
    args: same as ceteris_paribus method defined in line 50
    """
    @staticmethod
    def round_robin(
    p1,
    p2,
    session,
    approach,
    time_criterion= False,
    penality= False,
    krammer_simpson =False,
    fallback = "scoring",
    lineup_ranking= None):
        lineups_p1 = p1.lineups(session)
        lineups_p2 = p2.lineups(session)
        lineups_p1_not_p2, lineups_p2_not_p1 =  [], []
        #look for all the lineups of p1 where p2 doesn't belong
        for id in range(len(lineups_p1)):
            l1 = lineups_p1[id]
            tmp = [i for i in l1.players().values()]
            if p2.player_id not in tmp:
                lineups_p1_not_p2.append(l1)

        #look for all the lineups of p2 where p1 doesn't exist
        for id in range(len(lineups_p2)):
            l2 = lineups_p2[id]
            tmp = [i for i in l2.players().values()]
            if p1.player_id not in tmp:
                lineups_p2_not_p1.append(l2)
        counter = 0

        for l1 in lineups_p1_not_p2:
            for l2 in lineups_p2_not_p1:
                if(lineup_ranking is None):
                    #otherwise we use the ranking provided
                    w = LineUp.compare(l1, l2, session, approach, time_criterion, fallback)
                    if(w == 2):
                        counter -= 1
                    else:
                        counter += 1
                else:
                    #in case we're not using a predefined ranking we use the static method compare of the Lineup

                    rank_l1, rank_l2 = None , None
                    for j in lineup_ranking.keys():
                        if(l1.lineup_id in lineup_ranking[j]):
                            rank_l1 = j
                        if(l2.lineup_id in lineup_ranking[j]):
                            rank_l2 = j
                        if(rank_l1 is not None and rank_l2 is not None):
                            break

                    if(rank_l1 < rank_l2 ):
                        counter += 1
                    elif(rank_l1 > rank_l2 ):
                        counter -= 1
        #when using the krammer simpson methos we need the value of the comparaisions not just which player wins
        if krammer_simpson:
            return counter
        else:
            return 0 if counter == 0 else p1 if counter > 0 else p2
    """
    static method that is used to return the number of times
    P1 beats P2 and the number of times P2 beats P1
    These counters are then used to construct the initial matrix
    on which we apply the bradley and terry method
    args:
        p1(Player): first player to compare
        p2(Player): second player to compare
        session(sqlalchemy.orm.Session): session that allows connection to the database containing dataset
        approach(str): approach used to compare the lineups can be scoring , pareto_dominance , witness
        time_criterion(bool): wether to use time as a criterion when using the pareto domainance approach or not
        penality(bool): Assume if this boolean is set to true if some coalition C is unusable because only p1 (or p2)  played with it
            then p2 (the player that stops the ceteris paribus procedure from using this coalition) is penalized by counting the confrontation
            between the C U p2 as a loser against C U p1.
        lineup_ranking([lineUp]): ranking of lineups, if thie ranking is provided the comparaision between lineups
            will be done using this ranking insteaed of using the LineUp.compare method
    returns (int, int): (number of times p1 beats p2, number of times p2 beats p1)
    """
    @staticmethod
    def bradley_terry(p1, p2, session,
    approach,
    time_criterion= False,
    penality= True,
    fallback = "scoring",
    lineup_ranking= None):
        lineups_p1 = p1.lineups(session)
        lineups_p2 = p2.lineups(session)
        player_lp2, player_lp1_permut, player_lp1 =  [], [], []
        id1, id2 = [], []
        #look for all the lineups of p1 where p2 doesn't belong
        for id in range(len(lineups_p1)):
            l1 = lineups_p1[id]
            tmp = [i for i in l1.players().values()]
            if p2.player_id not in tmp:
                tmp.remove(p1.player_id)
                player_lp1.append(tmp)
                #permutation trick allows to consider all coalitions that contains the same players but don't play the same positions
                for i in permutations(tmp):
                    player_lp1_permut.append(list(i))
                    id1.append(id)
        #look for all the lineups of p2 where p1 doesn't exist
        for id in range(len(lineups_p2)):
            l2 = lineups_p2[id]
            tmp = [i for i in l2.players().values()]
            if p1.player_id not in tmp:
                tmp.remove(p2.player_id)
                player_lp2.append(tmp)
                id2.append(id)
        #use only the common lineups to compare
        common = [(player_lp1_permut.index(i),player_lp2.index(i)) for i in player_lp1_permut if i in player_lp2]
        #applying the penality
        if(penality):
            counter_p1, counter_p2 = len([i for i in player_lp1 if i not in common]) , len([i for i in player_lp2 if i not in common])
        else:
            counter_p1, counter_p2 = 0, 0
        if(lineup_ranking is None):
            #in case we're not using a predefined ranking we use the static method compare of the Lineup
            for i in common:
                w = LineUp.compare(lineups_p1[id1[i[0]]], lineups_p2[id2[i[1]]], session, approach, time_criterion, fallback)
                if(w == 2):
                    counter_p2 += 1
                else:
                    counter_p1 += 1
        else:
            #otherwise we use the ranking provided
            for i in common:
                #for each lineup in which both the players have played separetly
                #we compare the rank of those lineups and compare the players using the result of the previous comparision
                rank_l1, rank_l2 = None , None
                for j in lineup_ranking.keys():
                    if(lineups_p1[id1[i[0]]].lineup_id in lineup_ranking[j]):
                        rank_l1 = j
                    if(lineups_p2[id2[i[1]]].lineup_id in lineup_ranking[j]):
                        rank_l2 = j
                    if(rank_l1 is not None and rank_l2 is not None):
                        break
                if(rank_l1 < rank_l2 ):
                    counter_p1 += 1
                elif(rank_l1 > rank_l2 ):
                    counter_p2 += 1

        return counter_p1, counter_p2

    """
    static method that is used to compare two players p1 and p2
    using a lexicographic criteria
    args:
        p1(Player): first player to compare
        p2(Player): second player to compare
        front([lineUp]): ranking of lineups.
        session(sqlalchemy.orm.Session): session that allows connection to the database containing dataset

    returns (bool): p1 beats p2 ?
    """
    @staticmethod
    def l5_star(p1, p2,front ,session):
        #find all the lineups where p1 played
        lineups_p1 = [i.lineup_id for i in p1.lineups(session)]
        #find all the lineups where p2 played
        lineups_p2 = [i.lineup_id for i in p2.lineups(session)]

        done = False
        current_front = 0
        max = list(front.keys())[-1]
        #count the number of times each player is counted in
        #all the pareto fronts for
        #and stop as soon as we find that one player is more present than the other
        while(not done and current_front <= max):
            score_p1, score_p2= 0,0
            for i in lineups_p1:
                if i in front[current_front]:
                    score_p1 +=1
                    lineups_p1.remove(i)

            for i in lineups_p2:
                if i in front[current_front]:
                    score_p2 +=1
                    lineups_p2.remove(i)
            done = score_p1 != score_p2
            current_front += 1
        if(done):
            return p1 if score_p1 > score_p2 else p2
        else:
            return 0
    """
        method that returns all the lineups in which a player ever played
    """
    def lineups(self, session):
        return session.query(LineUp).filter(
            or_(
                LineUp.p1 == self.player_id,
                LineUp.p2 == self.player_id,
                LineUp.p3 == self.player_id,
                LineUp.p4 == self.player_id,
                LineUp.p5 == self.player_id,
            )
        ).all()
    """
        method that returns the time played by a player
    """
    def time_played(self, session):
        cumm_time = 0
        for i in self.lineups(session):
            cumm_time += i.score(session)[2]
        return cumm_time

    """
        This method codes a novel approach that aims to compare two players on the base of their contribution
        not only when they're added alone to a coalition of 4 members but also when they are part of a coalition C
        of n players and this coalition is added to another coaltion of size 5 - n.
        The aggregation operator used in our case is the average but this approach wasn't explored enough and
        the code deserves to be optmized and reviewed in order to use this approach more extensively.
    """
    def average_rankings(self, session, nb_team_mates = None, approach= "pareto_dominance", time_criterion = True, fallback=None):
        if(nb_team_mates > 4):
            raise ValueError("Player can't have more than 4 team mates at a time")
        team = session.query(Team).filter(Team.team_id == self.team).all()[0]

        players = team.players(session)
        team_mates = [i for i in players if i != self]
        if(nb_team_mates == None):
            pass
        else:
            score = 0
            #find all the possible coalition that can involve this player
            possible_coalitions = [list(i + (self,)) for i in combinations(team_mates, nb_team_mates)]
            nb_existing_coalitions = 0
            non_existing_coalitions = []
            coalition_scores, weights = [], []
            for coalition in possible_coalitions:
                #for every coalition that contains the current player
                #find all the lineups that contain both the players at the same time
                common_lineups = coalition[0].lineups(session)
                for player_index in range(1, len(coalition)):
                    tmp = coalition[player_index].lineups(session)
                    common_lineups = [i for i in common_lineups if i in tmp]
                #if there are common lineups then find all the possible composition of lineup that can involve this coalition
                if(len(common_lineups)):
                    nb_existing_coalitions +=1
                    possible_partial_lineups = combinations(
                        [i for i in players if i not in coalition],
                        4-nb_team_mates
                    )
                    score = 0
                    cpt = 0
                    for partial_lineup in possible_partial_lineups:
                        #for each possible composition , find all the lineups that contain this composition
                        lineups = partial_lineup[0].lineups(session)
                        for player_index in range(1, len(partial_lineup)):
                            tmp = partial_lineup[player_index].lineups(session)
                            lineups = [i for i in lineups if i in tmp]
                        #look for the lineup that contains the union of both coalitions
                        interesting_lineup = [i for i in lineups if i in common_lineups]
                        if(len(interesting_lineup)):
                            cpt+=1
                            interesting_lineup = interesting_lineup[0]
                            tmp = [i for i in lineups if i != interesting_lineup]
                            #compute the score given by a partial lineup to the coalition we are interested in
                            #score  =  number of coalitions of the same size that dominate this coalition
                            counter = 0
                            for lineup in tmp:
                                counter += LineUp.compare(interesting_lineup, lineup,session, approach, time_criterion, fallback)==2
                            score += counter
                        else:
                            pass

                    #cpt is never zero since if there are common lineups then there will be at least one coalition with whom those lineups played at least once
                    coalition_scores.append(score/(cpt))
                    weights.append(cpt)
                else:
                    non_existing_coalitions.append(coalition)
            non_existing_coalitions_score = nb_existing_coalitions
            for i in range(len(non_existing_coalitions)):
                weights.append(0)
                coalition_scores.append(non_existing_coalitions_score)

            return np.average(coalition_scores, weights = weights)
"""
Class used to represent the Team table and manipulate team entities
A Team is defined with the following fields:
id (int): unique id of the team_id
team_name (str); name of the team
"""
class Team(Base):
    __tablename__ = "team"
    team_id = Column(Integer, primary_key=True, autoincrement=True)
    team_name = Column(String, unique=True)

    """
        method that returns the players of a team
    """
    def players(self, session):
        return session.query(Player).filter(Player.team == self.team_id).all()

    """
        method that returns the lineups of a team
    """
    def lineups(self, session):
        return session.query(LineUp).filter(
            and_(Player.team == self.team_id,
                or_(
                    Player.player_id == LineUp.p1,
                    Player.player_id == LineUp.p2,
                    Player.player_id == LineUp.p3,
                    Player.player_id == LineUp.p4,
                    Player.player_id == LineUp.p5
                )
            )
        ).distinct().all()

    """
        method that ranks all the lineups of a team and stores the ranking as a dict containing all the equivalence classes in .bin file
        args:
            session(sqlalchemy.orm.Session): session that allows connection to the database containing dataset
            approach(str): approach used to compare the lineups , can be scoring , pareto_dominance or witness
            time_criterion(bool): wether to use time as a criterion when using the pareto domainance approach or not
            fallback(str): this parameter is made to be used when using the witness approach to
                specify how to compare two lineups in case they don't have any witness lineups
    """
    def rank_lineups(self, session, approach, time_criterion = False, fallback="scoring",bt = False):
        if(approach=="scoring"):
            lineups = self.lineups(session)
            front = {}
            current_level = 0
            #compute the score for all the lineups of that team
            tmp = [(i.score(session), i.lineup_id) for i in lineups]
            scores = [(i[1], i[0][0]- i[0][1]) for i in tmp]
            scores = sorted(scores, key = lambda i: i[1])
            equivalence_classes = self.__construct_equivalence_class__(np.array([i[0] for i in scores]))
            front = {}
            for i in range(len(equivalence_classes)):
                front[i] = []
                for j in equivalence_classes[i]:
                    front[i].append(lineups[j].lineup_id)
            tmp = front
        #if we're using the pareto_dominance we construct the equivalence classes directly
        #by taking all the items in the pareto front and putting them in the same equivalence class
        # then removing all of those lineups and repeating this process until all the lineups are ordered
        if(approach == "pareto_dominance"):
            lineups = self.lineups(session)
            front = {}
            current_level = 0
            while(len(lineups)):
                front[current_level] = []
                for i in range(len(lineups)):
                    dominated = False
                    for j in front[current_level]:
                        lineups_comparison = LineUp.compare(lineups[i], j, session, approach, time_criterion)
                        if(lineups_comparison == 2):
                            dominated = True
                            break
                        elif(lineups_comparison == 1):
                            front[current_level].remove(j)
                    if(not dominated):
                        front[current_level].append(lineups[i])

                for i in lineups:
                    if(i in front[current_level]):
                        lineups.remove(i)
                current_level+=1
            tmp = {}
            for i in front.keys():
                tmp[i] = [j.lineup_id for j in front[i]]
        #if we use the witness approach to compare the lineups
        #we start by generating the matrix of pair comparisions
        #then , since pair comparisions can result in non transitive preferences
        #we use the copeland method to break the cycles
        elif(approach == "witness"):
            lineups = self.lineups(session)
            #in case we use bradley and terry
            if(bt):
                lineups = self.lineups(session)
                # we start by creating the bradley and terry matrix
                #using the scores that are returned by the compare method of the LineUp class
                matrix = np.zeros((len(lineups), len(lineups)))
                for i in tqdm(range(len(lineups))):
                    for j in range(i+1, len(lineups)):
                        i_score, j_score = LineUp.compare(lineups[i], lineups[j], session,"witness", bradley_terry = True)
                        matrix[i,j] = i_score
                        matrix[j,i] = j_score
                #then we construct the ranking of the lineups
                equivalence_classes = bradley_terry(matrix, lineups, 200)
                front = {}
                for i in range(len(equivalence_classes)):
                    front[i] = []
                    for j in equivalence_classes[i]:
                        front[i].append(j)
                tmp = front
            else:
                ranking = np.zeros((len(lineups),len(lineups)))
                for i in tqdm(range(len(lineups))):
                    for j in range(i+1,len(lineups)):
                        comp_result = LineUp.compare(lineups[i], lineups[j],session, approach,time_criterion, fallback)
                        if(comp_result == 2):
                            ranking[i,j] = -1
                            ranking[j,i] = comp_result
                        else:
                            ranking[i,j] = comp_result
                            ranking[j,i] = -1
                equivalence_classes = self.__construct_equivalence_class__(np.sum(ranking,  axis = 1))
                front = {}
                for i in range(len(equivalence_classes)):
                    front[i] = []
                    for j in equivalence_classes[i]:
                        front[i].append(lineups[j].lineup_id)
                tmp = front
        #Save the rankings of the lineups in order to reuse them quickly
        with open("lineup_rankings/{0}_{1}_{2}_{3}.bin".
            format(
                self.team_name,approach,
                "time" if time_criterion and not approach =="scoring" else "no_time",
                "bt" if bt else "no_bt"
            ),"wb"
        ) as f:
            dump(tmp, f)


    def lineup_ranking(self, session, approach, time_criterion = False, fallback="scoring",bt = False):
        return self.__get_equivalence_class_from_file__(session , approach, time_criterion, fallback, bt)

    """
        method that ranks all the players of a team
        args:
            session(sqlalchemy.orm.Session): session that allows connection to the database containing dataset
            approach(str): approach used to compare the lineups , can be scoring , pareto_dominance or witness
            technique(method of player): technique to use when comparing players
            values can be Player.ceteris_paribus or Player.round_robin
            time_criterion(bool): wether to use time as a criterion when using the pareto domainance approach or not
            penality(bool): boolean used in the ceteris paribus function (see ceteris_paribus for more details)
            fallback(str): this parameter is made to be used when using the witness approach to
                specify how to compare two lineups in case they don't have any witness lineups
            krammer_simpson(bool): in case we're interested in breaking the cycles, indicated if we're using the krammer simpson method
            show_heatmap(bool): if true shows the heatmap of pairwise compararisions between players
    """
    def rank_players(
        self,
        session,
        technique,
        approach,
        time_criterion = False,
        penality = True,
        fallback= "scoring",
        bradley_terry = False,
        krammer_simpson = False,
        show_heatmap=False,
        show_progress = True):
        l5_star = False
        rr = technique == Player.round_robin
        if(technique == Player.l5_star or approach == "witness" or approach =="pareto_dominance"):
            front = self.__get_equivalence_class_from_file__(session, approach, time_criterion, fallback, bradley_terry)
            if(technique == Player.l5_star):
                technique = partial(technique, front = front)
                l5_star = True
            else:
                technique = partial(
                    technique,
                    approach = approach,
                    time_criterion = time_criterion,
                    penality = penality ,
                    krammer_simpson = krammer_simpson,
                    lineup_ranking = front
                )
        else:
            front = self.__get_equivalence_class_from_file__(session, approach, time_criterion, fallback, bradley_terry)
            technique = partial(
                technique,
                approach = approach,
                time_criterion = time_criterion,
                penality = penality ,
                krammer_simpson = krammer_simpson,
                lineup_ranking = front
            )
        if(rr):
            if(krammer_simpson):
                file_name = "round_robin/{0}_{1}_{2}_ks.bin".format(self.team_name, approach, "time" if  time_criterion and not approach == "scoring" else "no_time")
            else:
                file_name = "round_robin/{0}_{1}_{2}.bin".format(self.team_name, approach, "time" if  time_criterion and not approach == "scoring" else "no_time")
            try:
                with open(file_name,"rb") as f:
                    ranking = load(f)
                    if(show_heatmap):
                        ranking_to_heatmap(ranking, self.players(session))
                    return ranking
            except FileNotFoundError:
                pass
        players = self.players(session)
        ranking = np.zeros((len(players), len(players)), dtype = int)
        if show_progress:
            counter = tqdm(range(len(players)))
        else:
            counter = range(len(players))
        for i in counter:
            for j in range(i+1, len(players)):
                result = technique(p1 = players[i], p2 = players[j], session = session)
                if(krammer_simpson):
                    ranking[i, j] = result
                    ranking[j, i] = -result
                else:
                    if(result):
                        if(result == players[i]):
                            ranking[i, j] = 1
                            ranking[j, i] = -1
                        else:
                            ranking[i, j] = -1
                            ranking[j, i] = 1
        if(show_heatmap):
            ranking_to_heatmap(ranking, self.players(session))
        if(l5_star):
            return self.__construct_equivalence_class__(np.sum(ranking, axis = 1))
        else:

            if(rr):
                with open(file_name.format(self.team_name),"wb") as f:
                    dump(ranking,f)
                    if(show_heatmap):
                        ranking_to_heatmap(ranking, self.players(session))
            return ranking
    """
        method that ranks all the players of a team using the copeland method to break cycle in case there are any
        args:
            session(sqlalchemy.orm.Session): session that allows connection to the database containing dataset
            technique(method of player): technique to use when comparing players
            values can be Player.ceteris_paribus or Player.round_robin
            approach(str): approach used to compare the lineups , can be scoring , pareto_dominance or witness
            time_criterion(bool): wether to use time as a criterion when using the pareto domainance approach or not
            penality(bool): boolean used in the ceteris paribus function (see ceteris_paribus for more details)
            fallback(str): this parameter is made to be used when using the witness approach to
                specify how to compare two lineups in case they don't have any witness lineups
            krammer_simpson(bool): in case we're interested in breaking the cycles, indicated if we're using the krammer simpson method
            show_heatmap(bool): if true shows the heatmap of pairwise compararisions between players
    """
    def copeland(
        self,
        session,
        technique,
        approach,
        time_criterion = False,
        penality = True,
        fallback = None,
        bradley_terry = False,
        show_heatmap=False,
        show_progress = False):
        return self.__construct_equivalence_class__(
            np.sum(
                self.rank_players(
                    session,
                    technique,
                    approach,
                    time_criterion,
                    penality= penality,
                    krammer_simpson = False,
                    fallback = fallback,
                    bradley_terry = bradley_terry,
                    show_heatmap=show_heatmap,
                    show_progress = show_progress,
                ),
                axis = 1
            )
        )

    """
        Method that ranks all the players of a team using the krammer_simpson method to break cycle in case there are any
        args: same as copeland method defined in line 647.
    """
    def krammer_simpson(
        self,
        session,
        technique,
        approach,
        time_criterion = False,
        penality = True,
        fallback = None,
        bradley_terry = False,
        show_heatmap=False,
        show_progress = False):
        return self.__construct_equivalence_class__(
            np.min(
                self.rank_players(
                    session ,
                    technique,
                    approach,
                    time_criterion,
                    penality = penality,
                    krammer_simpson = True,
                    fallback = fallback,
                    bradley_terry = bradley_terry,
                    show_heatmap=show_heatmap,
                    show_progress = show_progress,
                ),
                axis = 1
            )
        )

    """
        Method that ranks all the players of a team using the top cycle method to break cycle in case there are any
        args: same as copeland method defined in line 647.
    """
    def connected_components_equiv(
        self,
        session,
        technique,
        approach,
        time_criterion=False,
        penality= True,
        fallback = None,
        bradley_terry = False,
        show_heatmap=False,
        show_progress = False):
        ranking = self.rank_players(
            session ,
            technique,
            approach,
            time_criterion,
            penality = penality,
            fallback = fallback,
            bradley_terry = bradley_terry,
            show_heatmap=show_heatmap,
            show_progress = show_progress,
        )
        players = self.players(session)
        graph = ranking_to_graph(ranking, players)
        indifferences = []
        #finding all the  cycles of the graph
        cycles = sorted(list(nx.simple_cycles(graph)), key = lambda i : len(i))
        if(not len(cycles)):
            return self.__construct_equivalence_class__(np.sum(ranking, axis=1))
        while(len(cycles)):
            #removing the shortest cycle of the graph and
            #repeating this procedure until there no more cycles in the graph
            indifferences.append(cycles[0])
            u = cycles[0][0]

            #merges the nodes that form a cycle into one node
            #returns a graph that contains only the u node (all the other nodes of that cycle are removed)
            #we dont't allow loops to form since loops don't have sense in our context
            for node in range(1,len(cycles[0])):
                graph = nx.contracted_nodes(graph,u, cycles[0][node], False)
            cycles = list(nx.simple_cycles(graph))
            cylces = sorted(cycles, key = lambda i : len(i))
        #constructing the equivalence classes of the ranking after removing the cycles
        equivalence_classes = []
        players = [i.name for i in players]
        while(graph.nodes):
            for i in nx.dag_longest_path(graph):
                tmp = [players.index(i)]
                for indif in indifferences:
                    if i in indif:
                        for j in range(1, len(indif)):
                            tmp.append(players.index(indif[j]))
                equivalence_classes.append(tmp)
                for i in tmp:
                    if(players[i] in graph.nodes):
                        graph.remove_node(players[i])
        return equivalence_classes
    """
        Method that ranks all the players of a team using the markov method to break cycle in case there are any
        args: same as copeland method defined in line 647.
    """
    def markov_based(
        self,
        session,
        technique,
        approach,
        time_criterion= False,
        penality= True,
        fallback= None,
        show_heatmap = False,
        show_progress= False):

        def steady_state_prop(p):
            dim = p.shape[0]
            q = (p-np.eye(dim))
            ones = np.ones(dim)
            q = np.c_[q,ones]
            QTQ = np.dot(q, q.T)
            bQT = np.ones(dim)
            return np.linalg.solve(QTQ,bQT)

        ranking = self.rank_players(
            session ,
            technique,
            approach,
            time_criterion,
            penality = penality,
            fallback = fallback,
            show_heatmap=show_heatmap,
            show_progress = show_progress,
        )
        players = self.players(session)

        markov_matrix = np.zeros((len(players), len(players)))

        for i in range(len(players)):
            markov_matrix[i,i] = len(np.where(ranking[i] < 0)[0])/(len(players) - 1)
            for j in np.where(ranking[i] > 0)[0]:
                markov_matrix[i,j] = 1/(len(players) - 1)
        steady_state_matrix = steady_state_prop(markov_matrix.transpose())

        return self.__construct_equivalence_class__(np.round(steady_state_matrix, 5))

    """
        method that is used to construct the ranking over the players using
        the bradley and terry method
    """
    def bradley_terry_player_ranking(self, session ,approach, time_criterion,  penality= False, fallback = None,show_heatmap=False,show_progress = False):
        players = self.players(session)
        matrix = np.zeros((len(players), len(players)), dtype=int)
        if(show_progress):
            progress = tqdm(range(len(players)))
        else:
            progress = range(len(players))
        for i in progress:
            for j in range(i+1, len(players)):
                res = Player.bradley_terry(players[i], players[j], session , approach, time_criterion, penality, fallback)
                matrix[i,j] = res[0]
                matrix[j,i] = res[1]
        if(show_heatmap):
            ranking_to_heatmap(matrix, players)
        return self.__construct_equivalence_class__(bradley_terry(matrix))
    def time_based_player_ranking(self, session):
        times = []
        for i in self.players(session):
            times.append((i.name,i.time_played(session)))
        return [i for i in reversed([[i[0]] for i in sorted(times, key = lambda item: item[1])])]

    def coalitionnal_votes_based_ranking(self, session):
        player_scores = []
        players = self.players(session)
        for id_ in tqdm(range(len(self.players(session)))):
            player = players[id_]
            tmp = 0
            for j in range(4):
                tmp += player.average_rankings(session, j)
            player_scores.append(-tmp)
        return self.__construct_equivalence_class__(np.array(player_scores))

    """
        method that returns the ranking of players using borda method
    """
    def borda_ranking(self, session, approach, time_criterion= False, fallback= None, bradley_terry = False):
        front = self.__get_equivalence_class_from_file__(session, approach, time_criterion, fallback, bradley_terry)
        player_scores = []
        nb_theorical_lineups = len([i for i in combinations(range(len(self.players(session))-1), 4)])
        for i in self.players(session):
            #give worst possible score to all the coalitions that a player can play with that don't exist
            tmp = (nb_theorical_lineups - len([j.lineup_id for j in i.lineups(session)]))* len(front)
            for j in i.lineups(session):
                for id_ in front:
                    if(j.lineup_id in front[id_]):
                        tmp+= id_+1
                        break
            player_scores.append(-tmp)
        return self.__construct_equivalence_class__(np.array(player_scores))

    """
        method that returns a ranking of the players using a scoring method based
        on the score of the lineups to which the players belong to
    """
    def scoring_ranking(self, session):
        player_scores = []
        nb_theorical_lineups = len([i for i in combinations(range(len(self.players(session))-1), 4)])
        for i in self.players(session):
            tmp = 0
            for j in i.lineups(session):
                tmp_scores = j.score(session)
                tmp +=  tmp_scores[0] - tmp_scores[1]
            player_scores.append(tmp)
        return self.__construct_equivalence_class__(np.array(player_scores))

    """
        method that returns a ranking of the players using STV method
    """
    def stv_based_ranking(self, session, approach, time_criterion, fallback, bradley_terry = False):
        """
            function that applies one iteration of the stv voting rule
        """
        def apply_stv(votes):
            tmp_votes = np.array(votes, dtype = int)
            ranking = []
            indices = np.array([i for i in range(votes.shape[1])])
            while(tmp_votes.shape[1]):
                strongest = find_best_alternative(tmp_votes)
                ranking.append([indices[strongest]])
                indices = np.delete(indices, strongest, 0)
                tmp_votes = update_votes(tmp_votes, strongest = strongest)
            players = self.players(session)
            return ranking
        """
            function that finds the best alternative given a voting profile
        """
        def find_best_alternative(votes):
            indices = np.array([i for i in range(votes.shape[1])])
            tmp_votes = np.array(votes)
            while(tmp_votes.shape[1]>1):
                #finding weakest alternative
                summed_tmp_votes = np.sum(tmp_votes, axis= 0)
                weakest = np.argmax(summed_tmp_votes)
                indices = np.delete(indices, weakest, 0)
                tmp_votes = update_votes(tmp_votes, weakest = weakest)
            return indices[0]
        """
            function that updates the votes after eliminating one alternative
        """
        def update_votes(votes, weakest =None, strongest = None):
            rand_id = np.random.randint(votes.shape[0])
            if weakest is not None:
                for vote in votes:
                    for i in range(len(vote)):
                        if vote[i] > vote[weakest]:
                            vote[i] -= 1
                votes = np.delete(votes, weakest, 1)

            if strongest is not None:
                for vote in votes:
                    for i in range(len(vote)):
                        if vote[i] > vote[strongest]:
                            vote[i] -= 1
                votes = np.delete(votes, strongest, 1)
            return votes

        """
            function that imports the votes from a file
            if file doesn't exist , generates the votes and returns them
        """
        def import_votes(players, alternatives):
            try:
                with open("stv_votes/{0}_{1}_{2}_{3}.bin"
                    .format(
                        self.team_name,
                        approach,
                        "time" if time_criterion and not approach == "scoring" else "no_time",
                        "bt" if bradley_terry else "no_bt"
                    ),
                    "rb"
                ) as f:
                    return load(f)
            except FileNotFoundError:
                generate_votes(players, alternatives)
                with open("stv_votes/{0}_{1}_{2}_{3}.bin"
                    .format(
                        self.team_name,
                        approach,
                        "time" if time_criterion and not approach == "scoring" else "no_time",
                        "bt" if bradley_terry else "no_bt"
                    ),
                    "rb"
                ) as f:
                    return load(f)
        """
            method that generates the votes of 4 members coalitions over the n-4 players of a team
        """
        def generate_votes(players, alternatives):
            front = self.__get_equivalence_class_from_file__(session, approach, time_criterion, fallback, bradley_terry)
            coalitions = [list(i) for i in combinations(players, 4)]
            votes = np.zeros((len(coalitions), len(alternatives)))
            lineups = self.lineups(session)
            real_coalitions = []
            coalition_lineups = []
            for l in lineups:
                lineup_players = l.players().values()
                for id_ in lineup_players:
                    if((tmp := [i for i in players if i.player_id in lineup_players and i.player_id != id_]) not in real_coalitions):
                        real_coalitions.append(tmp)
                        coalition_lineups.append([l])
                    else:
                        coalition_lineups[real_coalitions.index(tmp)].append(l)
            for c in tqdm(range(len(coalitions))):
                coalition = coalitions[c]
                if(coalition in real_coalitions):
                    tmp_coalition_lineups = coalition_lineups[real_coalitions.index(coalition)]
                    #generating all the lineups that a coalition can vote on
                    tmp = []
                    for a in alternatives:

                        #if player is in the coalitiont hen his score is 0 (best)
                        if players[a] not in coalition:
                            #otherwise we look for a common lineup between the player and the members of the coalition
                            #if this lineup exists than we let the coalition vote
                            #otherwise the score given for this player by this coalition is the max(worst)
                            done = False
                            for i in tmp_coalition_lineups:
                                if(players[a].player_id in i.players().values()):
                                    tmp.append(i)
                                    done = True
                                    break

                            if not done:
                                tmp.append(None)
                        else:
                            tmp.append(-1)
                else:
                    tmp = [None for i in range(len(alternatives))]
                lineup_scores = []
                #generating the scores of these lineups
                for i in range(len(tmp)):
                    if(tmp[i] is not None):
                        if(tmp[i] != -1):
                            for j in range(len(front)):
                                if tmp[i].lineup_id in front[j]:
                                    lineup_scores.append(j)
                                    break
                        else:
                            lineup_scores.append(0)
                    else:
                        lineup_scores.append(-1)
                lineup_scores = np.array(lineup_scores, dtype=int)
                last_rank = max(lineup_scores)+1
                lineup_scores= np.where(lineup_scores == -1, last_rank, lineup_scores)
                tmp_vote = []
                current_pos = 0
                for i in self.__construct_equivalence_class__(-lineup_scores):
                    for j in i:
                        votes[c, j] = current_pos
                    current_pos += len(i)
            with open("stv_votes/{0}_{1}_{2}_{3}.bin"
                .format(
                    self.team_name,
                    approach,
                    "time" if time_criterion and not approach == "scoring" else "no_time",
                    "bt" if bradley_terry else "no_bt"
                ),
                "wb"
            ) as f:
                dump(votes, f)
        players = self.players(session)
        votes = import_votes(players, range(len(players)))
        ranking = apply_stv(votes)
        return [i for i in ranking]
    """
        method that applies safety comparision
    """
    def safety_comparaision(self, session, rankings, show_heatmap=False):
        players = self.players(session)
        comparaision_matrix = np.zeros((len(players),len(players)), dtype = int)
        for i in range(len(players)):
            for j in range(i+1,len(players)):
                i_over_j, j_over_i = False, False
                for r in rankings:
                    r_i, r_j = None, None
                    for t in range(len(r)):
                        if i in r[t]:
                            r_i = t
                        if j in r[t]:
                            r_j = t
                        if r_i is not None and r_j is not None:
                            break
                    if(r_i is None):
                        if(r_j is not None):
                            j_over_i = True
                    else:
                        if(r_j is None):
                            i_over_j = True
                        else:
                            if(r_i < r_j):
                                i_over_j = True
                            if(r_i > r_j):
                                j_over_i = True
                if(j_over_i and i_over_j):
                    comparaision_matrix[i,j] = 2
                    comparaision_matrix[j,i] = 2
                else:
                    if(i_over_j):
                        comparaision_matrix[i,j] =1
                        comparaision_matrix[j,i] =-1
                    elif(j_over_i):
                        comparaision_matrix[i,j] =-1
                        comparaision_matrix[j,i] =1
                    else:
                        comparaision_matrix[i,j] =0
                        comparaision_matrix[j,i] =0
        if(show_heatmap):
            ranking_to_heatmap(comparaision_matrix, players)
        return comparaision_matrix
    """
        method that construct equivalence classes diven an unsorted array of points
        args:
            points(1d-array): UNSORTED array of the points of the individuals (items) we want to rank

        returns
            ([[int]]): list of lists , each list represents an equivalence class
    """
    def __construct_equivalence_class__(self,points):
        indices = np.array(range(len(points)))
        equivalence_classes = []
        while(points.shape[0]):
            max = np.max(points)
            tmp, tmp_indices = [],[]
            for i in range(points.shape[0]):
                if(points[i] == max):
                    tmp_indices.append(indices[i])
                    tmp.append(i)
            equivalence_classes.append(tmp_indices)
            cpt = 0
            for j in tmp:
                points = np.delete(points, j-cpt)
                indices = np.delete(indices, j-cpt)
                cpt+=1
        return equivalence_classes
    """
        Private method used to read the file that contains the equivalence classes
        args:

        returns
        [[int]] : list of lists , each list represents an equivalence class
    """
    def __get_equivalence_class_from_file__(self, session , approach, time_criterion, fallback, bradley_terry):
        try:
            with open("lineup_rankings/{0}_{1}_{2}_{3}.bin".
                format(
                    self.team_name,
                    approach,
                    "time" if time_criterion and not approach == "scoring" else "no_time",
                    "bt" if bradley_terry else "no_bt"
                    ),
                "rb"
            ) as f:
                return load(f)
        except FileNotFoundError:
            self.rank_lineups(session, approach, time_criterion, fallback, bradley_terry)
            with open("lineup_rankings/{0}_{1}_{2}_{3}.bin".
                format(
                    self.team_name,
                    approach,
                    "time" if time_criterion and not approach == "scoring" else "no_time",
                    "bt" if bradley_terry else "no_bt"
                    ),
                "rb"
            ) as f:
                return load(f)

    def critical_cases(self,session,reference_ranking,approach, time_criterion, fallback):
        players = self.players(session)
        base_db = sorted([i.name for i in players])
        tmp = ranking_to_ballot(index_ranking_to_name_ranking(self.copeland(session, Player.ceteris_paribus, approach, time_criterion = time_criterion, fallback = fallback,show_heatmap = False, show_progress = False), players), base_db)
        data_cop = {"copeland_cp":[base_db[i] for i in find_critical_case(self,reference_ranking,tmp, kendalltau_dist)]}
        df_cop = pd.DataFrame(data_cop)
        tmp = ranking_to_ballot(index_ranking_to_name_ranking(self.krammer_simpson(session, Player.ceteris_paribus, approach, time_criterion = time_criterion, fallback = fallback ,show_heatmap = False, show_progress = False), players), base_db)
        data_ks = {"KS_cp":[base_db[i] for i in find_critical_case(self,reference_ranking,tmp, kendalltau_dist)]}
        df_ks = pd.DataFrame(data_ks)
        tmp = ranking_to_ballot(index_ranking_to_name_ranking(self.connected_components_equiv(session, Player.ceteris_paribus, approach, time_criterion = time_criterion, fallback = fallback, show_heatmap = False, show_progress = False), players), base_db)
        data_kernel = {"kernel_cp":[base_db[i] for i in find_critical_case(self,reference_ranking,tmp, kendalltau_dist)]}
        df_kernel = pd.DataFrame(data_kernel)
        tmp = ranking_to_ballot(index_ranking_to_name_ranking(self.rank_players(session, Player.l5_star, approach, time_criterion = time_criterion, fallback = fallback,show_heatmap = False, show_progress = False), players), base_db)
        data_lex = {"lex":[base_db[i] for i in find_critical_case(self,reference_ranking,tmp, kendalltau_dist)]}
        df_lex = pd.DataFrame(data_lex)
        tmp = ranking_to_ballot(index_ranking_to_name_ranking(self.borda_ranking(session, approach, time_criterion = time_criterion, fallback = fallback), players), base_db)
        data_borda = {"borda":[base_db[i] for i in find_critical_case(self,reference_ranking,tmp, kendalltau_dist)]}
        df_borda = pd.DataFrame(data_borda)
        tmp = ranking_to_ballot(index_ranking_to_name_ranking(self.stv_based_ranking(session, approach, time_criterion = time_criterion, fallback = fallback), players), base_db)
        data_stv = {"stv":[base_db[i] for i in find_critical_case(self,reference_ranking,tmp, kendalltau_dist)]}
        df_stv = pd.DataFrame(data_stv)
        df = pd.concat([df_cop, df_ks, df_kernel, df_lex, df_borda, df_stv], axis = 1)
        df = df.fillna("/")
        return df

class LineUp(Base):
    __tablename__ = "lineup"
    lineup_id = Column(Integer, primary_key=True)
    p1 = Column(Integer, ForeignKey("player.player_id"))
    p2 = Column(Integer, ForeignKey("player.player_id"))
    p3 = Column(Integer, ForeignKey("player.player_id"))
    p4 = Column(Integer, ForeignKey("player.player_id"))
    p5 = Column(Integer, ForeignKey("player.player_id"))
    __table_args__ = (UniqueConstraint('p1', 'p2','p3','p4','p5', name='_lineup_composition_uc'),
    )

    def players(self):
        return {"p1" : self.p1, "p2" : self.p2, "p3" : self.p3, "p4" : self.p4, "p5" : self.p5}

    """
        method that returns all the scores of LineUp
        the scores of a lineup are list
    """
    def score(self, session):
        score = [0,0,0]
        for c in self.confrontations(session):
            score[2] += (c.start_time_m - c.end_time_m)*60 + (c.start_time_s - c.end_time_s)
            if(c.l1 == self.lineup_id):
                score[0] += c.home_score
                score[1] += c.away_score
            elif(c.l2 == self.lineup_id):
                score[0] += c.away_score
                score[1] += c.home_score
            score[0] /= (score[2]+1)
            score[1] /= (score[2]+1)
        return score

    """
        static method that is used to compare two lineups using several approaches of comparaision
        args:
        l1,l2(LineUp): the two lineups to compare
        session(sqlalchemy.orm.Session): session that allows connection to the database containing dataset
        approach(str): approach used to compare the lineups , can be scoring , pareto_dominance or witness
        time_criterion(bool): wether to use time as a criterion when using the pareto domainance approach or not
        fallback(str): this parameter is made to be used when using the witness approach to
            specify how to compare two lineups in case they don't have any witness lineups

        returns
            (int):
                0 if l1 and l2 are indifferenciable
                1 if l1 is preferred to l2
                2 if l2 is preferred to l1
    """
    @staticmethod
    def compare(l1, l2, session, approach, time_criterion = False, fallback=None, bradley_terry = False):
        score_l1 = l1.score(session)
        score_l2 = l2.score(session)
        if(approach == "pareto_dominance"):
            if(time_criterion):
                if(score_l1[0] == score_l2[0] and score_l1[1] == score_l2[1] and score_l1[2] == score_l2[2]):
                    return 0
                elif(score_l1[0] >= score_l2[0] and -score_l1[1] >= -score_l2[1] and score_l1[2] >= score_l2[2]):
                    return 1
                elif(score_l2[0] >= score_l1[0] and -score_l2[1] >= -score_l1[1] and score_l2[2] >= score_l1[2]):
                    return 2
                else:
                    return 0
            else:
                if(score_l1[0] == score_l2[0] and score_l1[1] == score_l2[1]):
                    return 0
                elif(score_l1[0] >= score_l2[0] and -score_l1[1] >= -score_l2[1]):
                    return 1
                elif(score_l2[0] >= score_l1[0] and -score_l2[1] >= -score_l1[1]):
                    return 2
                else:
                    return 0
        elif(approach == "scoring"):
            if(score_l1[2] == 0):
                if(score_l2[2] == 0):
                    return 0
                else:
                    return 2
            if(score_l2[2] == 0):
                if(score_l1[2] == 0):
                    return 0
                else:
                    return 1

            if((score_l1[0] - score_l1[1])/score_l1[2] == (score_l2[0] - score_l2[1])/score_l2[2]):
                return 0
            elif((score_l1[0] - score_l1[1])/score_l1[2] > (score_l2[0] - score_l2[1])/score_l2[2]):
                return 1
            else:
                return 2

        elif(approach == "witness"):
            witnesses = l1.indirect_confrontations(session, l2)
            l1_score, l2_score = [],[]
            #compute scores for both lineups against all witness lineups
            if(len(witnesses) or bradley_terry):
                for w in witnesses:
                    #scores for l1
                    tmp_score, tmp_cumm_time = 0, 0
                    for i in l1.confrontations(session,w):
                        tmp_cumm_time += (i.start_time_m - i.end_time_m - 1) * 60 + i.start_time_s - i.end_time_s + 60
                        if(i.lineups()[0] == l1.lineup_id):
                            tmp_score += i.home_score - i.away_score
                        else:
                            tmp_score += i.away_score - i.home_score
                    l1_score.append((tmp_score/(tmp_cumm_time+1), tmp_cumm_time))

                    #scores for l2
                    tmp_score, tmp_cumm_time = 0, 0
                    for i in l2.confrontations(session, w):
                        tmp_cumm_time += (i.start_time_m - i.end_time_m - 1) * 60 + i.start_time_s - i.end_time_s + 60
                        if(i.lineups()[0] == l2.lineup_id):
                            tmp_score += i.home_score - i.away_score
                        else:
                            tmp_score += i.away_score - i.home_score

                    l2_score.append((tmp_score/(tmp_cumm_time+1), tmp_cumm_time))

                #weighted vote using as weights the min time a witness spent against both of the lineups
                l1_votes , l2_votes = 0,0
                for i in range(len(witnesses)):
                    if bradley_terry:
                        if l1_score[i][0] > l2_score[i][0]:
                            l1_votes += 1
                        elif l1_score[i][0] < l2_score[i][0]:
                            l2_votes += 1
                    else:
                        if l1_score[i][0] > l2_score[i][0]:
                            l1_votes += min(l1_score[i][1] , l2_score[i][1])
                        elif l1_score[i][0] < l2_score[i][0]:
                            l2_votes += min(l1_score[i][1] , l2_score[i][1])
                if bradley_terry:
                    return l1_votes, l2_votes
                else:
                    if(l1_votes> l2_votes):
                        return 1
                    elif(l1_votes< l2_votes):
                        return 2
                    else:
                        return 0
            else:
                return LineUp.compare(l1, l2, session, fallback, time_criterion)
    """
        method that returns all the lineups that have ever been opposed to
        the self lineup
        returns
            [lineups] : list containing all the opponent lineups
    """
    def opponents(self, engine):
        opponents = []
        confrontations = self.confrontations(session)
        for i in confrontations:
            if(i.l1 == self.lineup_id):
                opponents.append(session.query(LineUp).filter(LineUp.lineup_id == i.l2).all()[0])
            else:
                opponents.append(session.query(LineUp).filter(LineUp.lineup_id == i.l1).all()[0])
        return opponents

    """
        method that returns all the lineups that were confronted (opposed) to both the self lineup
        and the opponent lineup given in the args.
        IMPORTANT:
        THE DEFINITION OF OPPONENT HERE IS A LINEUP OF THE SAME TEAM THAT WE ARE TRYING TO COMPARE TO
        THE SELF LINEUP (NOT A LINEUP WHO FACED THE SELF LINEUP)
        returns all the witness lineups  for a pair of lineup, opponent

        args:
            session(sqlalchemy.orm.Session): session that allows connection to the database containing dataset
            opponent(LineUp): oppoenent lineup that is considered when looking for witness lineups

        returns
            ([int]) list of integers that consist in the ids of the witness lineups
    """
    def indirect_confrontations(self, engine, opponent):
        sql = text("""
            with s1 as (select distinct(l2) as l2 from confrontation where l1 = {0}  UNION select distinct(l1) as l2 from confrontation where l2 = {0}),
            s2 as (select distinct(l2) as l2 from confrontation where l1 = {1}  UNION select distinct(l1) as l2 from confrontation where l2 = {1})
            select * from s1 INTERSECT select * from s2;
            """.format(self.lineup_id, opponent.lineup_id))
        results = engine.execute(sql)
        return [i[0] for i  in results]

    """
    method that returns all the confrontations of a lineup
    args:
        session(sqlalchemy.orm.Session): session that allows connection to the database containing dataset
        opponent(int): id of the opposing lineup in case we only want the confrontations of the
            current lineup with that specific opposing lineup

    returns:
    Iterable that consists of the entirety of confrontations for that lineup (against opponent lineup if opponent is given)
    """
    def confrontations(self, session, opponent= None):
        if(opponent is None):
            return session.query(Confrontation).filter(
                or_(
                    Confrontation.l1 == self.lineup_id,
                    Confrontation.l2 == self.lineup_id,
                )
            ).all()
        else:
            return session.query(Confrontation).filter(
                or_(
                    and_(
                        Confrontation.l1 == self.lineup_id,
                        Confrontation.l2 == opponent,
                    ),
                    and_(
                        Confrontation.l1 == opponent,
                        Confrontation.l2 == self.lineup_id)

                )
            ).all()
"""
Confrontation class used to represent the confrontation table
and manipulate confrontation entities
A confrontation is characterized by the following fields:
l1 (int): lineup id of the home team
l2 (int): lineup id of the waya team
home_team (int): team id of the home team
away_team (int): team id of the away team
home_score (int): score of the lineup of the home team during this confrontation
away_score (int): score of the lineup of the away team during this confrontation
match_date (datetime) : date of the match
start_time_m (int): minute at which this confrontation started
start_time_s (int): second at which this confrontation started
end_time_m (int): minute at which this confrontation ended
end_time_s (int): second at which this confrontation ended
quarter (int): quarter during which the confrontation happened
"""
class Confrontation(Base):
    __tablename__ = "confrontation"
    confrontation_id = Column(Integer, primary_key=True)
    l1 = Column(Integer, ForeignKey("lineup.lineup_id"))
    l2 = Column(Integer, ForeignKey("lineup.lineup_id"))
    home_team = Column(Integer, ForeignKey("team.team_id"))
    away_team = Column(Integer, ForeignKey("team.team_id"))
    home_score = Column(Integer)
    away_score = Column(Integer)
    match_date = Column(DateTime)
    start_time_m = Column(Integer)
    start_time_s = Column(Integer)
    end_time_m = Column(Integer)
    end_time_s = Column(Integer)
    quarter = Column(Integer)

    def lineups(self):
        return [self.l1,self.l2]

    """
    Static method that returns the totality of matches that were played during the season
    args
        session(sqlalchemy.orm.Session): session that enables a connection to a database
    """
    @staticmethod
    def matches(session):
        return [(i.match_date,i.home_team, i.away_team) for i in session.query(Confrontation.match_date,Confrontation.home_team, Confrontation.away_team).distinct(
            Confrontation.match_date,Confrontation.home_team, Confrontation.away_team
        ).all()]
