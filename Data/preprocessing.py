"""
    File that defines the necessary functions and main code for
    preprocessing the dataset and creating a database containing the
    data needed for the study
"""
import pandas as pd
import os
from sys import argv
from Model import engine , Player, Confrontation, LineUp, Team
from sqlalchemy.orm import sessionmaker
from datetime import date
from tqdm import tqdm
import sqlalchemy

"""
    function that adds a player to a the player table of the database
    args:
        player_name(str): name of the player to add
        team_name (str): name of the team where the player plays
"""
def add_player(player_name, team_name):
    global PLAYERS
    id = "{0} {1}".format(player_name, team_name)
    if(id not in PLAYERS):
        p = Player(name = player_name, team = TEAMS[team_name])
        session.add(p)
        session.commit()
        PLAYERS[id] = (p.player_id, team_name, player_name)

"""
    function that adds a team to the Team table of the database
    args:
        team_name (str): name of the team
"""
def add_team(team_name):
    global TEAMS
    if(team_name not in TEAMS):
        t = Team(team_name = team_name)
        session.add(t)
        session.commit()
        TEAMS[team_name] = t.team_id

"""
    function that checks if a lineup is valid
    a lineup is valid if all the players of that lineup are all registered in the player table
    and all of them play for the same team
    args:
        lineup ([PLayer]): list of players that compose the lienup
"""
def check_lineup_validity(lineup):
    global PLAYERS
    t = PLAYERS["{0} {1}".format(lineup[0][2], lineup[0][1])][1]
    for p in lineup:
        if(p[1] != t):
            print(lineup)
            print(p[2], p[1], t)
            raise ValueError

"""
    function that parses a row from the csv files
    row (dataframe): row of the csv file
    last (Bool): boolean that tells if the current row is the last row of a file
"""
def parse_row(row, last = False):
    global HOME_TEAM , AWAY_TEAM
    global PLAYERS, TEAMS, LINEUPS
    global CURRENT_LINEUP_AWAY, CURRENT_LINEUP_HOME, CURRENT_HOME_SCORE, CURRENT_AWAY_SCORE
    global CURRENT_START_TM,CURRENT_START_TS, CURRENT_END_TM, CURRENT_END_TS
    global CURRENT_QUARTER

    #add the players of the lineup of the away team
    add_player(row["a1"],AWAY_TEAM)
    add_player(row["a2"],AWAY_TEAM)
    add_player(row["a3"],AWAY_TEAM)
    add_player(row["a4"],AWAY_TEAM)
    add_player(row["a5"],AWAY_TEAM)

    #add the players of the lineup of the home team
    add_player(row["h1"],HOME_TEAM)
    add_player(row["h2"],HOME_TEAM)
    add_player(row["h3"],HOME_TEAM)
    add_player(row["h4"],HOME_TEAM)
    add_player(row["h5"],HOME_TEAM)

    #update the current lineups if new file is treated
    if(len(CURRENT_LINEUP_AWAY)!=5):
        CURRENT_LINEUP_AWAY = [
            PLAYERS["{0} {1}".format(row["a1"], AWAY_TEAM)],
            PLAYERS["{0} {1}".format(row["a2"], AWAY_TEAM)],
            PLAYERS["{0} {1}".format(row["a3"], AWAY_TEAM)],
            PLAYERS["{0} {1}".format(row["a4"], AWAY_TEAM)],
            PLAYERS["{0} {1}".format(row["a5"], AWAY_TEAM)]]

    if(len(CURRENT_LINEUP_HOME)!=5):
        CURRENT_LINEUP_HOME = [
            PLAYERS["{0} {1}".format(row["h1"], HOME_TEAM)],
            PLAYERS["{0} {1}".format(row["h2"], HOME_TEAM)],
            PLAYERS["{0} {1}".format(row["h3"], HOME_TEAM)],
            PLAYERS["{0} {1}".format(row["h4"], HOME_TEAM)],
            PLAYERS["{0} {1}".format(row["h5"], HOME_TEAM)]]

    #SUB event or new quarter event
    if(row["etype"] == "sub" or row["period"]!= CURRENT_QUARTER or last):
        lineup_home, lineup_away = None, None
        #if lineup doesn't already exist in the database
        if(CURRENT_LINEUP_HOME not in [i[0] for i in LINEUPS]):
            #check if the lineup is valid
            check_lineup_validity(CURRENT_LINEUP_HOME)
            #add the lineup to the database
            lineup_home = LineUp(
                p1 = PLAYERS["{0} {1}".format(CURRENT_LINEUP_HOME[0][2],CURRENT_LINEUP_HOME[0][1])][0],
                p2 = PLAYERS["{0} {1}".format(CURRENT_LINEUP_HOME[1][2],CURRENT_LINEUP_HOME[1][1])][0],
                p3 = PLAYERS["{0} {1}".format(CURRENT_LINEUP_HOME[2][2],CURRENT_LINEUP_HOME[2][1])][0],
                p4 = PLAYERS["{0} {1}".format(CURRENT_LINEUP_HOME[3][2],CURRENT_LINEUP_HOME[3][1])][0],
                p5 = PLAYERS["{0} {1}".format(CURRENT_LINEUP_HOME[4][2],CURRENT_LINEUP_HOME[4][1])][0])
            session.add(lineup_home)
            session.commit()
            LINEUPS.append((CURRENT_LINEUP_HOME,lineup_home.lineup_id))

        if(CURRENT_LINEUP_AWAY not in [i[0] for i in LINEUPS]):
            check_lineup_validity(CURRENT_LINEUP_AWAY)
            lineup_away = LineUp(
                p1 = PLAYERS["{0} {1}".format(CURRENT_LINEUP_AWAY[0][2],CURRENT_LINEUP_AWAY[0][1])][0],
                p2 = PLAYERS["{0} {1}".format(CURRENT_LINEUP_AWAY[1][2],CURRENT_LINEUP_AWAY[1][1])][0],
                p3 = PLAYERS["{0} {1}".format(CURRENT_LINEUP_AWAY[2][2],CURRENT_LINEUP_AWAY[2][1])][0],
                p4 = PLAYERS["{0} {1}".format(CURRENT_LINEUP_AWAY[3][2],CURRENT_LINEUP_AWAY[3][1])][0],
                p5 = PLAYERS["{0} {1}".format(CURRENT_LINEUP_AWAY[4][2],CURRENT_LINEUP_AWAY[4][1])][0])
            session.add(lineup_away)
            session.commit()
            LINEUPS.append((CURRENT_LINEUP_AWAY,lineup_away.lineup_id))
        #get the time at which the event happens
        tmp_m, tmp_s = 0, 0
        #if the event is a new period then we don't update the minutes and seconds and let them equal 0
        #otherwise we update them to have the value of the time field of the row
        if(int(row["period"]) == CURRENT_QUARTER):
            [tmp_m , tmp_s] = [int(i) for i in map(int,row["time"].split(":"))]

        #adding the confrontation to the databse
        if(lineup_home is None):
            for i in range(len(LINEUPS)):
                if CURRENT_LINEUP_HOME == LINEUPS[i][0]:
                    #balkis
                    lineup_home = LINEUPS[i][1]
                    break
        else:
            lineup_home = lineup_home.lineup_id

        if(lineup_away is None):
            for i in range(len(LINEUPS)):
                if CURRENT_LINEUP_AWAY ==  LINEUPS[i][0]:
                    lineup_away = LINEUPS[i][1]
                    break
        else:
            lineup_away = lineup_away.lineup_id

        c = Confrontation(
            l1 = lineup_home,
            l2 = lineup_away,
            home_team = HOME_TEAM,
            away_team = AWAY_TEAM,
            home_score = CURRENT_HOME_SCORE,
            away_score = CURRENT_AWAY_SCORE,
            match_date = MATCH_DATE,
            start_time_m = CURRENT_START_TM,
            start_time_s = CURRENT_START_TS,
            end_time_m =  tmp_m,
            end_time_s =  tmp_s,
            quarter = CURRENT_QUARTER)

        session.add(c)
        session.commit()

        #update the lineups after with the new value of the lineups after the event happened
        CURRENT_LINEUP_AWAY = [
            PLAYERS["{0} {1}".format(row["a1"], AWAY_TEAM)],
            PLAYERS["{0} {1}".format(row["a2"], AWAY_TEAM)],
            PLAYERS["{0} {1}".format(row["a3"], AWAY_TEAM)],
            PLAYERS["{0} {1}".format(row["a4"], AWAY_TEAM)],
            PLAYERS["{0} {1}".format(row["a5"], AWAY_TEAM)]]


        CURRENT_LINEUP_HOME = [
            PLAYERS["{0} {1}".format(row["h1"], HOME_TEAM)],
            PLAYERS["{0} {1}".format(row["h2"], HOME_TEAM)],
            PLAYERS["{0} {1}".format(row["h3"], HOME_TEAM)],
            PLAYERS["{0} {1}".format(row["h4"], HOME_TEAM)],
            PLAYERS["{0} {1}".format(row["h5"], HOME_TEAM)]]

        #reset time and score to 0
        CURRENT_HOME_SCORE, CURRENT_AWAY_SCORE = 0, 0
        if(row["period"]!= CURRENT_QUARTER):
            CURRENT_START_TM,CURRENT_START_TS, CURRENT_END_TM, CURRENT_END_TS = 12,00,0,0
        else:
            CURRENT_START_TM,CURRENT_START_TS, CURRENT_END_TM, CURRENT_END_TS = tmp_m,tmp_s,0,0

    CURRENT_QUARTER = int(row["period"])

    #SHOT MADE
    if(row["etype"] == "shot" and row["result"] == "made"):
        if(row["team"]  == AWAY_TEAM):
            CURRENT_AWAY_SCORE += int(row["points"])

        elif(row["team"] == HOME_TEAM):
            CURRENT_HOME_SCORE += int(row["points"])

        else:
            print(row["player"], CURRENT_LINEUP_AWAY, CURRENT_LINEUP_HOME)
            raise ValueError

    #FREE THROW
    if(row["etype"] == "free throw" and row["result"] == "made"):
        if(row["team"]  == AWAY_TEAM):
            CURRENT_AWAY_SCORE += 1

        elif(row["team"]  == HOME_TEAM):
            CURRENT_HOME_SCORE += 1

        else:
            print(row["player"], CURRENT_LINEUP_AWAY, CURRENT_LINEUP_HOME)
            raise ValueError

if __name__ == "__main__":
    #dict of the players and the teams that exist in the databse
    #list of lineups that exist in the database
    #(we use these dicts and this list to avoid reaching out to the database every time we need to check if a row exists in a certain table)
    global PLAYERS, TEAMS, LINEUPS
    PLAYERS, TEAMS, LINEUPS = {}, {}, []
    global CURRENT_LINEUP_AWAY, CURRENT_LINEUP_HOME, CURRENT_AWAY_SCORE, CURRENT_HOME_SCORE
    global CURRENT_START_TM,CURRENT_START_TS, CURRENT_END_TM, CURRENT_END_TS, MATCH_DATE
    global CURRENT_QUARTER
    global HOME_TEAM , AWAY_TEAM

    Session = sessionmaker()
    Session.configure(bind=engine)
    session = Session()


    path =r'./{0}'.format(argv[1])
    list_of_files = []

    #construct the list of files in the dataset
    for root, dirs, files in os.walk(path):
        for file in files:
            if(file.endswith(".csv")):
                list_of_files.append(os.path.join(root,file))


    for i in tqdm(range(len(list_of_files))):
        #reset the values of team, lineup, score, time and quarter every time a we start processing a new file
        CURRENT_LINEUP_AWAY, CURRENT_LINEUP_HOME, CURRENT_HOME_SCORE, CURRENT_AWAY_SCORE = [], [], 0, 0
        CURRENT_START_TM,CURRENT_START_TS, CURRENT_END_TM, CURRENT_END_TS = 12,0,0,0
        CURRENT_QUARTER = 1
        HOME_TEAM , AWAY_TEAM = None, None
        name = list_of_files[i]
        df = pd.read_csv(name)
        tmp = name[len(path):]
        #extracting match date , home team and away team
        MATCH_DATE ,AWAY_TEAM, HOME_TEAM = date(int(tmp.split(".")[0][:4]),int(tmp.split(".")[0][4:6]), int(tmp.split(".")[0][6:])), tmp.split(".")[1][:3], tmp.split(".")[1][3:]
        add_team(HOME_TEAM)
        add_team(AWAY_TEAM)
        for row in range(df.shape[0]):
            parse_row(df.iloc[row], row == df.shape[0] - 1)
