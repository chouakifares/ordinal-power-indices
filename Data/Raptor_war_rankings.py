"""
    File that defines the necessary functions and the main code
    for extracting the references rankings from the files containing
    the players' RAPTORS and WAR scores and creating the rankings of
    the players with respect to these indices.
"""
import pandas as pd
from Model import Team
from sys import argv
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
if __name__ == "__main__":
    engine = create_engine("sqlite:///{0}".format(argv[1]),echo = False)

    Session = sessionmaker()
    Session.configure(bind=engine)

    global session
    session = Session()


    teams = session.query(Team).all()

    keys = ["predator_total", "war_reg_season"]
    df = pd.read_csv("Raptor_ranking.csv", index_col=False)

    for t in teams:
        for k in keys:
            if(k == "predator_total"):
                name = "RAPTOR"
            else:
                name = "WAR"
            to_write = df.loc[
                (df["season"] == 2010) &
                (df["season_type"] == "RS") &
                (df["team"] == t.team_name)
            ][["player_name", k]].sort_values(
                k,
                ascending=False
            ).reset_index(drop = True)
            to_write.rename(
                columns = {"player_name":""},
                inplace = True
            )
            to_write.index.names = ['Rk']
            to_write.to_csv(
                "NBA_player_rankings/{0}/{0}_{1}.csv".
                format(t.team_name,name)
            )
