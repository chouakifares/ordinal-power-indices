"""
********************************************WARNING**************************************
This file deletes all the tables of the database and recreates them
Do not run this file unless you're sure you want to reset the database and repopulate it again
it is preferable to backup the database before running this script
If you want to keep the current database and create a new one you should change the value of the DB_NAME variable.
"""

from termcolor import colored, cprint
from sqlalchemy import  create_engine

from Model import Base


DB_NAME = "test.db"
if __name__ == "__main__":
    cprint("CURRENT OPERATION :", "blue", end="")
    cprint("creating database engine")
    engine = create_engine("sqlite:///{0}".format(DB_NAME),echo = False)
    cprint("SUCCESS :", "green", end="")
    cprint("database engine created")


    cprint("CURRENT OPERATION :", "blue", end="")
    cprint("Dropping all tables of the previous database")
    Base.metadata.drop_all(bind=engine)
    cprint("SUCCESS :", "green", end="")
    cprint("All previous tables have been dropped")

    cprint("CURRENT OPERATION :", "blue", end="")
    cprint("Creating new tables")

    cprint("\t CURRENT SUB-OPERATION :", "blue", end="")
    cprint("Creating new table team")
    Base.metadata.tables["team"].create(bind = engine)
    cprint("\t Success :", "green", end="")
    cprint("Table team created")

    cprint("\t CURRENT SUB-OPERATION :", "blue", end="")
    cprint("Creating new table player")
    Base.metadata.tables["player"].create(bind = engine)
    cprint("\t Success :", "green", end="")
    cprint("Table player created")

    cprint("\t CURRENT SUB-OPERATION :", "blue", end="")
    cprint("Creating new table lineup")
    Base.metadata.tables["lineup"].create(bind = engine)
    cprint("\t Success :", "green", end="")
    cprint("Table lineup created")

    cprint("\t CURRENT SUB-OPERATION :", "blue", end="")
    cprint("Creating new table confrontation")
    Base.metadata.tables["confrontation"].create(bind = engine)
    cprint("\t Success :", "green", end="")
    cprint("Table confrontation created")

    cprint("SUCCESS :", "green", end="")
    cprint("All tables are now created")
