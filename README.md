
# Ordinal power indices : an experiment with basketball dataset 

This project is a realized in the context of my 3 months internship, which is a mandatory requierment for me to pass my 1st year of the ANDROIDE master. It is supervised by Ms. Aurélie Beynier, Mr. Nicolas Maudet and Ms. Meltem Öztürk.
## Keywords
Multiagent systems, Social power, Coalitional power, Ordinal power, Coalition formation, Basketball,  Performance modeling.
  
## Background    
Recently, the notion of ordinal power indices has been put forward in the literature.  In a nutshell, the problem is as follows:  given a ranking over coalitions of agents (or objets), how can we derive a ranking over singleton.<sup>1</sup>  
  
For instance, this may be a question faced by the manager of a basketball team $$T =  \{t _1 , t_2, . . . , t_m\}$$ who we would like to derive an ordering over players $t_i$ (for instance to allocate medical staff to prevent injuries).  
  
Basketball is an interesting setting, since many different line-ups(subset of 5 players) are used throughout a given game.  Also, there are a lot of available data for this sport.  NBA play-by-play data is available,  see for instance [1,2]and [3] for an example of how such data can be exploited in an AI context.  
  
Compared to the theoretical framework of ordinal power indices studied in [4,5,6], it is however important to emphasize some key differences:  
1. The observations that are available in the dataset do not consist of direct comparisons of line-ups of the same team $T$.  Instead, each line-up $T_i ⊂ T$ is opposed to different line-ups of other teams.  
2. The order is partial.  In particular, only coalitions of size 5 are compared(and not all possible coalitions of size 5 are actually occurring, as there are constraints on positions filled by players).

<sub><sup>1</sup> This is the main research question investigated in the ANR-funded project THEMIS.</sub>
## To Do
1. Curate data from NBA datasets to obtain, for a target team $T$, a comparison of line-ups of this team. A basic idea, to compare two line-ups $T_i$ and $T_j$ could be to rely on strictly identical witnesses: each line-up $O_i$ of an opposing team which has been opposed to both $T_i$ and $T_j$ may be a witness which can vote either for $T_i$ or $T_j$, depending on how well they performed (relatively) against $O_i$. Challenges already arise for this basic task, as for instance line-ups may be of varying length. By aggregating the votes of the different witnesses, we obtain our pairwise comparison of $T_i$ vs. $T_j$. Overall we get a tournament of comparisons of the (existing) line-ups of $T$ (with perhaps some weighting to account for the confidencein the comparison). However there is no guarantee that this tournament would be transitive (it is very unlikely to be).
2. Apply ordinal power indices methods to retrieve a ranking on players of $T$. We may rely on existing techniques, such as CP-majority or Lex-Cel (in which case the tournament retrieved in 1. would need to be made transitive), or propose original approaches specifically dedicated to the setting studied.
3. Present synthetically the results obtained, comparing the different techniques (possible on different target teams). 
## References 
[1]  http://www.basketball-reference.com
[2]  http://www.basketballgeek.com
[3]  Adversarial Synergy Graph Model for Predicting Game Outcomes in Hu-man Basketball Somchaya Liemhetcharat and Yicheng Luo Proceedingsof the AAMAS 2015 Workshop on Adaptive and Learning Agents (ALA2015), May 2015
[4]  Moretti, S. and Öztürk, M., “Some axiomatic and algorithmic perspectiveson the social ranking problem,” in International Conference on Algorith-mic DecisionTheory, pp. 166–181, Springer, 2017.
[5]  A. Haret, H. Khani, Moretti, S., and Öztürk, M., “Ceteris paribus ma-jority for social ranking.,” in Proceedings of the 27th International JointConference on Artificial Intelligence (IJCAI 2018), pp. 303–309, 2018.
[6]  E. Algaba, S. Moretti, E. R ́emila, P. Solal (2021) Lexicographic solutionsfor coalitional rankings, Soc Choice Welf, 57, 817–849.

